# Test Suite

Where all the project tests are centralized.

## Test Coverage

It uses [Cobertura](https://cobertura.github.io/cobertura/) format report about how extensive the tests are.
