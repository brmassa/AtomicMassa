namespace AtomicMassa.Engine.Core.Graphics;

/// <summary>
/// Represents a Vulkan swap chain for rendering frames to a window or surface.
/// </summary>
public class SwapChain : IDisposable
{
    /// <summary>
    /// Maximum number of frames to be processed concurrently.
    /// </summary>
    public const int maxframesinflight = 2;

    /// <summary>
    /// Gets the maximum frames in flight.
    /// </summary>
    public static int MaxFramesInFlight => maxframesinflight;

    /// <summary>
    /// Gets the Vulkan swap chain.
    /// </summary>
    public SwapchainKHR VkSwapChain => swapChain;

    /// <summary>
    /// Gets the format of the swap chain images.
    /// </summary>
    public Format SwapChainImageFormat => swapChainImageFormat;

    /// <summary>
    /// Gets the format of the depth component in the swap chain.
    /// </summary>
    public Format SwapChainDepthFormat => swapChainDepthFormat;

    /// <summary>
    /// Gets an array of image views for the swap chain images.
    /// </summary>
    /// <returns>An array of image views.</returns>
    public ImageView[] GetSwapChainImageViews() => swapChainImageViews;

    /// <summary>
    /// Gets the framebuffer at a specified index for rendering.
    /// </summary>
    /// <param name="i">The index of the framebuffer to retrieve.</param>
    /// <returns>The framebuffer at the specified index.</returns>
    public Framebuffer GetFrameBufferAt(uint i) => swapChainFramebuffers[i];

    /// <summary>
    /// Gets an array of framebuffers for rendering.
    /// </summary>
    /// <returns>An array of framebuffers.</returns>
    public Framebuffer[] GetFrameBuffers() => swapChainFramebuffers;

    /// <summary>
    /// Gets the count of framebuffers in the swap chain.
    /// </summary>
    /// <returns>The count of framebuffers.</returns>
    public uint GetFrameBufferCount() => (uint)swapChainFramebuffers.Length;

    /// <summary>
    /// Indicates whether FIFO swapping is used.
    /// </summary>
    public bool UseFifo { get; set; }

    /// <summary>
    /// Gets the width of the swap chain.
    /// </summary>
    public uint Width => swapChainExtent.Width;

    /// <summary>
    /// Gets the height of the swap chain.
    /// </summary>
    public uint Height => swapChainExtent.Height;

    /// <summary>
    /// Gets the extent of the swap chain.
    /// </summary>
    /// <returns>The extent of the swap chain.</returns>
    public Extent2D SwapChainExtent => swapChainExtent;

    /// <summary>
    /// Gets the aspect ratio of the swap chain.
    /// </summary>
    /// <returns>The aspect ratio of the swap chain.</returns>
    public float AspectRatio => (float)swapChainExtent.Width / swapChainExtent.Height;

    /// <summary>
    /// Gets the render pass of the swap chain.
    /// </summary>
    /// <returns>The render pass of the swap chain.</returns>
    public RenderPass RenderPass => renderPass;

    private readonly Vk vk;
    private readonly Device device;
    private readonly Silk.NET.Vulkan.Device vkDevice;
    private KhrSwapchain khrSwapChain = null!;
    private SwapchainKHR swapChain;
    private Image[] swapChainImages = null!;
    private Format swapChainDepthFormat;
    private Extent2D swapChainExtent;
    private ImageView[] swapChainImageViews = null!;
    private Framebuffer[] swapChainFramebuffers = null!;
    private RenderPass renderPass;
    private Image[] depthImages = null!;
    private DeviceMemory[] depthImageMemorys = null!;
    private ImageView[] depthImageViews = null!;
    private Image[] colorImages = null!;
    private DeviceMemory[] colorImageMemorys = null!;
    private ImageView[] colorImageViews = null!;
    private Extent2D windowExtent;
    private Silk.NET.Vulkan.Semaphore[] imageAvailableSemaphores = null!;
    private Silk.NET.Vulkan.Semaphore[] renderFinishedSemaphores = null!;
    private Fence[] inFlightFences = null!;
    private Fence[] imagesInFlight = null!;
    private int currentFrame;
    private Format swapChainImageFormat;
    private readonly SwapChain? oldSwapChain = null!;

    /// <summary>
    /// Gets the image count of the swap chain.
    /// </summary>
    /// <returns>The image count of the swap chain.</returns>
    public uint ImageCount() => (uint)swapChainImageViews.Length;

    /// <summary>
    /// Initializes a new instance of the <see cref="SwapChain"/> class.
    /// </summary>
    /// <param name="vk">The Vulkan API object.</param>
    /// <param name="device">The device object.</param>
    /// <param name="extent">The extent of the swap chain.</param>
    /// <param name="useFifo">Whether to use Fifo for the swap chain.</param>
    public SwapChain(Vk vk, Device device, Extent2D extent, bool useFifo)
    {
        ArgumentNullException.ThrowIfNull(device);

        this.vk = vk;
        this.device = device;
        UseFifo = useFifo;
        vkDevice = device.VkDevice;
        windowExtent = extent;
        Initialize();
    }

    /// <summary>
    /// Acquires the next image in the swap chain.
    /// </summary>
    /// <param name="imageIndex">The index of the image to acquire.</param>
    /// <returns>The result of the acquire operation.</returns>
    public Result AcquireNextImage(ref uint imageIndex)
    {
        _ = vk.WaitForFences(device.VkDevice, 1, inFlightFences[currentFrame], true, ulong.MaxValue);

        var result = khrSwapChain.AcquireNextImage(
            device.VkDevice,
            swapChain,
            ulong.MaxValue,
            imageAvailableSemaphores[currentFrame],
            default,
            ref imageIndex
        );

        return result;
    }

    /// <summary>
    /// Submits the command buffers for execution.
    /// </summary>
    /// <param name="commandBuffer">The command buffer to submit.</param>
    /// <param name="imageIndex">The index of the image in the swap chain.</param>
    /// <returns>The result of the submission.</returns>
    /// <remarks>
    /// This method performs several tasks:
    /// - Waits for the fences before acquiring the next image.
    /// - Assigns in-flight images.
    /// - Configures and submits the submit info.
    /// - Resets the fences.
    /// - Handles the presentation of the queue.
    /// This method is unsafe because it uses pointers and unmanaged memory operations.
    /// </remarks>
    public unsafe Result SubmitCommandBuffers(CommandBuffer commandBuffer, uint imageIndex)
    {
        if (imagesInFlight![imageIndex].Handle != default)
        {
            _ = vk!.WaitForFences(device.VkDevice, 1, imagesInFlight[imageIndex], true, ulong.MaxValue);
        }
        imagesInFlight[imageIndex] = inFlightFences[currentFrame];

        SubmitInfo submitInfo = new() { SType = StructureType.SubmitInfo, };

        var waitSemaphores = stackalloc[] { imageAvailableSemaphores[currentFrame] };
        var waitStages = stackalloc[] { PipelineStageFlags.ColorAttachmentOutputBit };

        submitInfo = submitInfo with
        {
            WaitSemaphoreCount = 1,
            PWaitSemaphores = waitSemaphores,
            PWaitDstStageMask = waitStages,
            CommandBufferCount = 1,
            PCommandBuffers = &commandBuffer
        };

        var signalSemaphores = stackalloc[] { renderFinishedSemaphores![currentFrame] };
        submitInfo = submitInfo with
        {
            SignalSemaphoreCount = 1,
            PSignalSemaphores = signalSemaphores,
        };

        _ = vk!.ResetFences(device.VkDevice, 1, inFlightFences[currentFrame]);

        if (
            vk!.QueueSubmit(device.GraphicsQueue, 1, submitInfo, inFlightFences[currentFrame])
            != Result.Success
        )
        {
            throw new VulkanException("failed to submit draw command buffer!");
        }

        var swapChains = stackalloc[] { swapChain };
        PresentInfoKHR presentInfo =
            new()
            {
                SType = StructureType.PresentInfoKhr,
                WaitSemaphoreCount = 1,
                PWaitSemaphores = signalSemaphores,
                SwapchainCount = 1,
                PSwapchains = swapChains,
                PImageIndices = &imageIndex
            };

        var result = khrSwapChain.QueuePresent(device.PresentQueue, presentInfo);

        currentFrame = (currentFrame + 1) % maxframesinflight;

        return result;
    }

    /// <summary>
    /// Dispose buffers
    /// </summary>
    public unsafe void Dispose()
    {
        foreach (var framebuffer in swapChainFramebuffers)
        {
            vk.DestroyFramebuffer(device.VkDevice, framebuffer, null);
        }

        foreach (var imageView in swapChainImageViews)
        {
            vk.DestroyImageView(device.VkDevice, imageView, null);
        }
        Array.Clear(swapChainImageViews);

        for (var i = 0; i < depthImages.Length; i++)
        {
            vk.DestroyImageView(device.VkDevice, depthImageViews[i], null);
            vk.DestroyImage(device.VkDevice, depthImages[i], null);
            vk.FreeMemory(device.VkDevice, depthImageMemorys[i], null);
        }

        for (var i = 0; i < colorImages.Length; i++)
        {
            vk.DestroyImageView(device.VkDevice, colorImageViews[i], null);
            vk.DestroyImage(device.VkDevice, colorImages[i], null);
            vk.FreeMemory(device.VkDevice, colorImageMemorys[i], null);
        }

        vk.DestroyRenderPass(device.VkDevice, renderPass, null);

        // cleanup synchronization objects
        for (var i = 0; i < maxframesinflight; i++)
        {
            vk.DestroySemaphore(device.VkDevice, renderFinishedSemaphores[i], null);
            vk.DestroySemaphore(device.VkDevice, imageAvailableSemaphores[i], null);
            vk.DestroyFence(device.VkDevice, inFlightFences[i], null);
        }

        khrSwapChain!.DestroySwapchain(device.VkDevice, swapChain, null);
        swapChain = default;

        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Initializes the swap chain.
    /// </summary>
    private void Initialize()
    {
        CreateSwapChain();
        CreateImageViews();
        CreateRenderPass();
        CreateColorResources();
        CreateDepthResources();
        CreateFrameBuffers();
        CreateSyncObjects();
    }

    private unsafe void CreateSwapChain()
    {
        var swapChainSupport = device.QuerySwapChainSupport;

        var surfaceFormat = ChooseSwapSurfaceFormat(swapChainSupport.Formats);
        var presentMode = ChoosePresentMode(swapChainSupport.PresentModes);
        var extent = ChooseSwapExtent(swapChainSupport.Capabilities);

        var imageCount = swapChainSupport.Capabilities.MinImageCount + 1;
        if (
            swapChainSupport.Capabilities.MaxImageCount > 0
            && imageCount > swapChainSupport.Capabilities.MaxImageCount
        )
        {
            imageCount = swapChainSupport.Capabilities.MaxImageCount;
        }

        SwapchainCreateInfoKHR creatInfo =
            new()
            {
                SType = StructureType.SwapchainCreateInfoKhr,
                Surface = device.Surface,
                MinImageCount = imageCount,
                ImageFormat = surfaceFormat.Format,
                ImageColorSpace = surfaceFormat.ColorSpace,
                ImageExtent = extent,
                ImageArrayLayers = 1,
                ImageUsage = ImageUsageFlags.ColorAttachmentBit,
            };

        var indices = device.FindQueueFamilies;
        var queueFamilyIndices = stackalloc[] {
            indices.GraphicsFamily!.Value,
            indices.PresentFamily!.Value
        };

        if (indices.GraphicsFamily != indices.PresentFamily)
        {
            creatInfo = creatInfo with
            {
                ImageSharingMode = SharingMode.Concurrent,
                QueueFamilyIndexCount = 2,
                PQueueFamilyIndices = queueFamilyIndices,
            };
        }
        else
        {
            creatInfo.ImageSharingMode = SharingMode.Exclusive;
        }

        creatInfo = creatInfo with
        {
            PreTransform = swapChainSupport.Capabilities.CurrentTransform,
            CompositeAlpha = CompositeAlphaFlagsKHR.OpaqueBitKhr,
            PresentMode = presentMode,
            Clipped = true,
        };

        if (khrSwapChain is null)
        {
            if (!vk.TryGetDeviceExtension(device.Instance, vkDevice, out khrSwapChain))
            {
                throw new NotSupportedException("VK_KHR_swapchain extension not found.");
            }
        }

        creatInfo.OldSwapchain = oldSwapChain == default ? default : oldSwapChain.VkSwapChain;

        var resultVulkan = khrSwapChain.CreateSwapchain(vkDevice, creatInfo, null, out swapChain);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to create swap chain: {0}", resultVulkan);
        }

        _ = khrSwapChain.GetSwapchainImages(vkDevice, swapChain, ref imageCount, null);
        swapChainImages = new Image[imageCount];
        fixed (Image* swapChainImagesPtr = swapChainImages)
        {
            _ = khrSwapChain.GetSwapchainImages(
                vkDevice,
                swapChain,
                ref imageCount,
                swapChainImagesPtr
            );
        }

        swapChainImageFormat = surfaceFormat.Format;
        swapChainExtent = extent;
    }

    private unsafe void CreateImageViews()
    {
        swapChainImageViews = new ImageView[swapChainImages.Length];

        for (var i = 0; i < swapChainImages.Length; i++)
        {
            ImageViewCreateInfo createInfo =
                new()
                {
                    SType = StructureType.ImageViewCreateInfo,
                    Image = swapChainImages[i],
                    ViewType = ImageViewType.Type2D,
                    Format = swapChainImageFormat,
                    SubresourceRange =
                    {
                        AspectMask = ImageAspectFlags.ColorBit,
                        BaseMipLevel = 0,
                        LevelCount = 1,
                        BaseArrayLayer = 0,
                        LayerCount = 1,
                    }
                };

            if (
                vk.CreateImageView(vkDevice, createInfo, null, out swapChainImageViews[i])
                != Result.Success
            )
            {
                throw new VulkanException("failed to create image view!");
            }
        }
    }

    private unsafe void CreateRenderPass()
    {
        AttachmentDescription depthAttachment =
            new()
            {
                Format = device.FindDepthFormat(),
                Samples = device.MsaaSamples,
                LoadOp = AttachmentLoadOp.Clear,
                StoreOp = AttachmentStoreOp.DontCare,
                StencilLoadOp = AttachmentLoadOp.DontCare,
                StencilStoreOp = AttachmentStoreOp.DontCare,
                InitialLayout = ImageLayout.Undefined,
                FinalLayout = ImageLayout.DepthStencilAttachmentOptimal,
            };

        AttachmentReference depthAttachmentRef =
            new() { Attachment = 1, Layout = ImageLayout.DepthStencilAttachmentOptimal, };

        AttachmentDescription colorAttachment =
            new()
            {
                Format = swapChainImageFormat,
                Samples = device.MsaaSamples,
                LoadOp = AttachmentLoadOp.Clear,
                StoreOp = AttachmentStoreOp.Store,
                StencilLoadOp = AttachmentLoadOp.DontCare,
                StencilStoreOp = AttachmentStoreOp.DontCare,
                InitialLayout = ImageLayout.Undefined,
                FinalLayout = ImageLayout.ColorAttachmentOptimal,
            };

        AttachmentReference colorAttachmentRef =
            new() { Attachment = 0, Layout = ImageLayout.ColorAttachmentOptimal, };

        AttachmentDescription colorAttachmentResolve =
            new()
            {
                Format = swapChainImageFormat,
                Samples = SampleCountFlags.Count1Bit,
                LoadOp = AttachmentLoadOp.DontCare,
                StoreOp = AttachmentStoreOp.Store,
                StencilLoadOp = AttachmentLoadOp.DontCare,
                StencilStoreOp = AttachmentStoreOp.DontCare,
                InitialLayout = ImageLayout.Undefined,
                FinalLayout = ImageLayout.PresentSrcKhr
            };

        AttachmentReference colorAttachmentResolveRef =
            new() { Attachment = 2, Layout = ImageLayout.AttachmentOptimalKhr };

        SubpassDescription subpass =
            new()
            {
                PipelineBindPoint = PipelineBindPoint.Graphics,
                ColorAttachmentCount = 1,
                PColorAttachments = &colorAttachmentRef,
                PDepthStencilAttachment = &depthAttachmentRef,
                PResolveAttachments = &colorAttachmentResolveRef
            };

        SubpassDependency dependency =
            new()
            {
                DstSubpass = 0,
                DstAccessMask =
                    AccessFlags.ColorAttachmentWriteBit
                    | AccessFlags.DepthStencilAttachmentWriteBit,
                DstStageMask =
                    PipelineStageFlags.ColorAttachmentOutputBit
                    | PipelineStageFlags.EarlyFragmentTestsBit,
                SrcSubpass = Vk.SubpassExternal,
                SrcAccessMask = 0,
                SrcStageMask =
                    PipelineStageFlags.ColorAttachmentOutputBit
                    | PipelineStageFlags.EarlyFragmentTestsBit,
            };

        var attachments = new[] { colorAttachment, depthAttachment, colorAttachmentResolve };

        fixed (AttachmentDescription* attachmentsPtr = attachments)
        {
            RenderPassCreateInfo renderPassInfo =
                new()
                {
                    SType = StructureType.RenderPassCreateInfo,
                    AttachmentCount = (uint)attachments.Length,
                    PAttachments = attachmentsPtr,
                    SubpassCount = 1,
                    PSubpasses = &subpass,
                    DependencyCount = 1,
                    PDependencies = &dependency,
                };

            if (
                vk.CreateRenderPass(vkDevice, renderPassInfo, null, out renderPass)
                != Result.Success
            )
            {
                throw new VulkanException("failed to create render pass!");
            }
        }
    }

    private unsafe void CreateFrameBuffers()
    {
        swapChainFramebuffers = new Framebuffer[swapChainImageViews.Length];

        for (var i = 0; i < swapChainImageViews.Length; i++)
        {
            var attachments = new[]
            {
                colorImageViews[i],
                depthImageViews[i],
                swapChainImageViews[i]
            };

            fixed (ImageView* attachmentsPtr = attachments)
            {
                FramebufferCreateInfo framebufferInfo =
                    new()
                    {
                        SType = StructureType.FramebufferCreateInfo,
                        RenderPass = renderPass,
                        AttachmentCount = (uint)attachments.Length,
                        PAttachments = attachmentsPtr,
                        Width = swapChainExtent.Width,
                        Height = swapChainExtent.Height,
                        Layers = 1,
                    };

                var resultVulkan = vk.CreateFramebuffer(device.VkDevice, framebufferInfo, null, out swapChainFramebuffers[i]);
                if (resultVulkan != Result.Success)
                {
                    throw new VulkanException("Vulkan: failed to create framebuffer: {0}", resultVulkan);
                }
            }
        }
    }

    private unsafe void CreateColorResources()
    {
        var colorFormat = swapChainImageFormat;

        var imageCount = ImageCount();
        colorImages = new Image[imageCount];
        colorImageMemorys = new DeviceMemory[imageCount];
        colorImageViews = new ImageView[imageCount];

        for (var i = 0; i < imageCount; i++)
        {
            ImageCreateInfo imageInfo =
                new()
                {
                    SType = StructureType.ImageCreateInfo,
                    ImageType = ImageType.Type2D,
                    Extent =
                    {
                        Width = swapChainExtent.Width,
                        Height = swapChainExtent.Height,
                        Depth = 1,
                    },
                    MipLevels = 1,
                    ArrayLayers = 1,
                    Format = colorFormat,
                    Tiling = ImageTiling.Optimal,
                    InitialLayout = ImageLayout.Undefined,
                    Usage =
                        ImageUsageFlags.TransientAttachmentBit | ImageUsageFlags.ColorAttachmentBit,
                    Samples = device.MsaaSamples,
                    SharingMode = SharingMode.Exclusive,
                    Flags = 0
                };

            fixed (Image* imagePtr = &colorImages[i])
            {
                var resultVulkan = vk.CreateImage(vkDevice, imageInfo, null, imagePtr);
                if (resultVulkan != Result.Success)
                {
                    throw new VulkanException("Vulkan: failed to create color image: {0}", resultVulkan);
                }
            }

            vk.GetImageMemoryRequirements(vkDevice, colorImages[i], out var memRequirements);

            MemoryAllocateInfo allocInfo =
                new()
                {
                    SType = StructureType.MemoryAllocateInfo,
                    AllocationSize = memRequirements.Size,
                    MemoryTypeIndex = device.FindMemoryType(
                        memRequirements.MemoryTypeBits,
                        MemoryPropertyFlags.DeviceLocalBit
                    ),
                };

            fixed (DeviceMemory* imageMemoryPtr = &colorImageMemorys[i])
            {
                var resultVulkan = vk.AllocateMemory(vkDevice, allocInfo, null, imageMemoryPtr);
                if (resultVulkan != Result.Success)
                {
                    throw new VulkanException("Vulkan: failed to allocate color image memory: {0}", resultVulkan);
                }
            }

            _ = vk.BindImageMemory(vkDevice, colorImages[i], colorImageMemorys[i], 0);

            // color image view
            ImageViewCreateInfo createInfo =
                new()
                {
                    SType = StructureType.ImageViewCreateInfo,
                    Image = colorImages[i],
                    ViewType = ImageViewType.Type2D,
                    Format = colorFormat,
                    SubresourceRange =
                    {
                        AspectMask = ImageAspectFlags.ColorBit,
                        BaseMipLevel = 0,
                        LevelCount = 1,
                        BaseArrayLayer = 0,
                        LayerCount = 1,
                    }
                };

            if (
                vk.CreateImageView(vkDevice, createInfo, null, out colorImageViews[i])
                != Result.Success
            )
            {
                throw new VulkanException("failed to create color image views!");
            }
        }
    }

    private unsafe void CreateDepthResources()
    {
        var depthFormat = device.FindDepthFormat();
        swapChainDepthFormat = depthFormat;

        var imageCount = ImageCount();
        depthImages = new Image[imageCount];
        depthImageMemorys = new DeviceMemory[imageCount];
        depthImageViews = new ImageView[imageCount];

        for (var i = 0; i < imageCount; i++)
        {
            ImageCreateInfo imageInfo =
                new()
                {
                    SType = StructureType.ImageCreateInfo,
                    ImageType = ImageType.Type2D,
                    Extent =
                    {
                        Width = swapChainExtent.Width,
                        Height = swapChainExtent.Height,
                        Depth = 1,
                    },
                    MipLevels = 1,
                    ArrayLayers = 1,
                    Format = depthFormat,
                    Tiling = ImageTiling.Optimal,
                    InitialLayout = ImageLayout.Undefined,
                    Usage = ImageUsageFlags.DepthStencilAttachmentBit,
                    Samples = device.MsaaSamples,
                    SharingMode = SharingMode.Exclusive,
                    Flags = 0
                };

            fixed (Image* imagePtr = &depthImages[i])
            {
                var resultVulkan = vk.CreateImage(vkDevice, imageInfo, null, imagePtr);
                if (resultVulkan != Result.Success)
                {
                    throw new VulkanException("Vulkan: failed to create depth image: {0}", resultVulkan);
                }
            }

            vk.GetImageMemoryRequirements(vkDevice, depthImages[i], out var memRequirements);

            MemoryAllocateInfo allocInfo =
                new()
                {
                    SType = StructureType.MemoryAllocateInfo,
                    AllocationSize = memRequirements.Size,
                    MemoryTypeIndex = device.FindMemoryType(
                        memRequirements.MemoryTypeBits,
                        MemoryPropertyFlags.DeviceLocalBit
                    ),
                };

            fixed (DeviceMemory* imageMemoryPtr = &depthImageMemorys[i])
            {
                var resultVulkan = vk.AllocateMemory(vkDevice, allocInfo, null, imageMemoryPtr);
                if (resultVulkan != Result.Success)
                {
                    throw new VulkanException("vulkan: failed to allocate depth image memory: {0}", resultVulkan);
                }
            }

            _ = vk.BindImageMemory(vkDevice, depthImages[i], depthImageMemorys[i], 0);

            // depth image view
            ImageViewCreateInfo createInfo =
                new()
                {
                    SType = StructureType.ImageViewCreateInfo,
                    Image = depthImages[i],
                    ViewType = ImageViewType.Type2D,
                    Format = depthFormat,
                    SubresourceRange =
                    {
                        AspectMask = ImageAspectFlags.DepthBit,
                        BaseMipLevel = 0,
                        LevelCount = 1,
                        BaseArrayLayer = 0,
                        LayerCount = 1,
                    }
                };

            if (
                vk.CreateImageView(vkDevice, createInfo, null, out depthImageViews[i])
                != Result.Success
            )
            {
                throw new VulkanException("failed to create depth image views!");
            }
        }
    }

    private unsafe void CreateImage(
        uint width,
        uint height,
        uint mipLevels,
        SampleCountFlags numSamples,
        Format format,
        ImageTiling tiling,
        ImageUsageFlags usage,
        MemoryPropertyFlags properties,
        ref Image image,
        ref DeviceMemory imageMemory
    )
    {
        ImageCreateInfo imageInfo =
            new()
            {
                SType = StructureType.ImageCreateInfo,
                ImageType = ImageType.Type2D,
                Extent =
                {
                    Width = width,
                    Height = height,
                    Depth = 1,
                },
                MipLevels = mipLevels,
                ArrayLayers = 1,
                Format = format,
                Tiling = tiling,
                InitialLayout = ImageLayout.Undefined,
                Usage = usage,
                Samples = numSamples,
                SharingMode = SharingMode.Exclusive,
            };

        fixed (Image* imagePtr = &image)
        {
            var resultVulkan = vk.CreateImage(vkDevice, imageInfo, null, imagePtr);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to create image: {0}", resultVulkan);
            }
        }

        vk.GetImageMemoryRequirements(vkDevice, image, out var memRequirements);

        MemoryAllocateInfo allocInfo =
            new()
            {
                SType = StructureType.MemoryAllocateInfo,
                AllocationSize = memRequirements.Size,
                MemoryTypeIndex = device.FindMemoryType(memRequirements.MemoryTypeBits, properties),
            };

        fixed (DeviceMemory* imageMemoryPtr = &imageMemory)
        {
            var resultVulkan = vk.AllocateMemory(vkDevice, allocInfo, null, imageMemoryPtr);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to allocate image memory: {0}", resultVulkan);
            }
        }

        _ = vk.BindImageMemory(vkDevice, image, imageMemory, 0);
    }

    private unsafe void CreateSyncObjects()
    {
        imageAvailableSemaphores = new Silk.NET.Vulkan.Semaphore[maxframesinflight];
        renderFinishedSemaphores = new Silk.NET.Vulkan.Semaphore[maxframesinflight];
        inFlightFences = new Fence[maxframesinflight];
        imagesInFlight = new Fence[swapChainImages!.Length];

        SemaphoreCreateInfo semaphoreInfo = new() { SType = StructureType.SemaphoreCreateInfo, };

        FenceCreateInfo fenceInfo =
            new() { SType = StructureType.FenceCreateInfo, Flags = FenceCreateFlags.SignaledBit, };

        for (var i = 0; i < maxframesinflight; i++)
        {
            var resultVulkan = vk.CreateSemaphore(vkDevice, semaphoreInfo, null, out imageAvailableSemaphores[i]);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to create synchronization objects for a frame: {0}", resultVulkan);
            }
            resultVulkan = vk.CreateSemaphore(vkDevice, semaphoreInfo, null, out renderFinishedSemaphores[i]);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to create synchronization objects for a frame: {0}", resultVulkan);
            }
            resultVulkan = vk.CreateFence(vkDevice, fenceInfo, null, out inFlightFences[i]);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to create synchronization objects for a frame: {0}", resultVulkan);
            }
        }
    }

    private static SurfaceFormatKHR ChooseSwapSurfaceFormat(
        IReadOnlyList<SurfaceFormatKHR> availableFormats
    )
    {
        foreach (var availableFormat in availableFormats)
        {
            if (
                availableFormat.Format == Format.B8G8R8A8Srgb
                && availableFormat.ColorSpace == ColorSpaceKHR.SpaceSrgbNonlinearKhr
            )
            {
                return availableFormat;
            }
        }

        return availableFormats[0];
    }

    private PresentModeKHR ChoosePresentMode(IReadOnlyList<PresentModeKHR> availablePresentModes)
    {
        if (UseFifo)
        {
            return PresentModeKHR.FifoKhr;
        }

        foreach (var availablePresentMode in availablePresentModes)
        {
            if (availablePresentMode == PresentModeKHR.MailboxKhr)
            {
                Log.Logger.Lap("swapchain", $"got present mode = Mailbox");
                return availablePresentMode;
            }
        }

        Log.Logger.Lap("swapchain", $"fallback present mode = FifoKhr");
        return PresentModeKHR.FifoKhr;
    }

    private Extent2D ChooseSwapExtent(SurfaceCapabilitiesKHR capabilities)
    {
        if (capabilities.CurrentExtent.Width != uint.MaxValue)
        {
            return capabilities.CurrentExtent;
        }
        else
        {
            var framebufferSize = windowExtent;

            Extent2D actualExtent =
                new() { Width = framebufferSize.Width, Height = framebufferSize.Height };

            actualExtent.Width = Math.Clamp(
                actualExtent.Width,
                capabilities.MinImageExtent.Width,
                capabilities.MaxImageExtent.Width
            );
            actualExtent.Height = Math.Clamp(
                actualExtent.Height,
                capabilities.MinImageExtent.Height,
                capabilities.MaxImageExtent.Height
            );

            return actualExtent;
        }
    }
}
