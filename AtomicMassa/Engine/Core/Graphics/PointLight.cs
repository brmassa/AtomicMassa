namespace AtomicMassa.Engine.Core.Graphics;

/// <summary>
/// Represents a point light source in 3D space.
/// </summary>
public struct PointLight
{
    private Vector4D<float> position = Vector4D<float>.Zero;
    private Vector4D<float> color = Vector4D<float>.One;

    /// <summary>
    /// Initializes a new instance of the <see cref="PointLight"/> struct with default values.
    /// </summary>
    public PointLight() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="PointLight"/> struct with the specified position and color.
    /// </summary>
    /// <param name="position">The position of the point light.</param>
    /// <param name="color">The color of the point light.</param>
    public PointLight(Vector4D<float> position, Vector4D<float> color)
    {
        this.position = position;
        this.color = color;
    }

    /// <summary>
    /// Sets the position of the point light.
    /// </summary>
    /// <param name="pos">The new position of the point light.</param>
    public void SetPosition(Vector3D<float> pos) =>
        position = new Vector4D<float>(pos.X, pos.Y, pos.Z, 0f);

    /// <summary>
    /// Sets the color and intensity of the point light.
    /// </summary>
    /// <param name="col">The color of the point light.</param>
    /// <param name="intensity">The intensity of the point light.</param>
    public void SetColor(Vector4D<float> col, float intensity) =>
        color = new Vector4D<float>(col.X, col.Y, col.Z, intensity);

    /// <summary>
    /// Converts the <see cref="PointLight"/> to a byte array.
    /// </summary>
    /// <returns>A byte array representing the <see cref="PointLight"/>.</returns>
    public readonly byte[] GetAsBytes()
    {
        var bytes = new byte[32];
        position.AsBytes().CopyTo(bytes, 0);
        color.AsBytes().CopyTo(bytes, 16);

        return bytes;
    }

    /// <summary>
    /// Gets the size of the <see cref="PointLight"/> in bytes.
    /// </summary>
    /// <returns>The size of the <see cref="PointLight"/> in bytes.</returns>
    public static uint SizeOf => (uint)Unsafe.SizeOf<PointLight>();

    /// <inheritdoc />
    public override readonly string ToString()
    {
        return $"p:{position}, c:{color}";
    }

    /// <inheritdoc />
	public override bool Equals(object? obj) => obj switch
    {
        PointLight other => Equals(other),
        _ => false,
    };

    /// <inheritdoc />
	public static bool operator ==(PointLight left, PointLight right) => left.Equals(right);

    /// <inheritdoc />
	public static bool operator !=(PointLight left, PointLight right) => !(left == right);

    /// <inheritdoc />
    public override int GetHashCode() => position.GetHashCode();
}
