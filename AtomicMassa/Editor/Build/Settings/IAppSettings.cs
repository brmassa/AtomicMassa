namespace AtomicMassa.Editor.Build.Settings;

/// <summary>
/// The centralized settings of the application.
/// </summary>
public interface IAppSettings
{
    /// <summary>
    /// Current project asset.json.
    ///
    /// It's not serialized, but set in realtime after these settings are loaded successfully.
    /// </summary>
    public string? AssetJsonAbsolutePath { get; set; }

    /// <summary>
    /// Current project path.
    ///
    /// It's not serialized, but set in realtime after these settings are loaded successfully.
    /// </summary>
    public string ProjectAbsoluteDir { get; }

    /// <summary>
    /// Current project Asset path.
    ///
    /// It's not serialized, but set in realtime after these settings are loaded successfully.
    /// </summary>
    public string AssetsAbsoluteDir { get; }

    /// <summary>
    /// Project title.
    /// </summary>
    public string? Title { get; set; }

    /// <summary>
    /// Convert the Tile to use only path-friendly characters
    /// </summary>
    public string? TitleToPathFriendly { get; }

    /// <summary>
    /// Essentially to clone or absorve settings from other object.
    /// </summary>
    /// <param name="appSettings"></param>
    /// <returns></returns>
    public IAppSettings Load(IAppSettings appSettings);
}
