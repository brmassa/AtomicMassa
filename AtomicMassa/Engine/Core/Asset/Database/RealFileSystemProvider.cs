namespace AtomicMassa.Engine.Core.Asset.Database;

/// <summary>
/// Provides access to an asset file located in the real file system.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="RealFileSystemProvider"/> class with the specified file path.
/// </remarks>
/// <param name="filePath">The file path of the asset in the real file system.</param>
public class RealFileSystemProvider(string filePath) : IAssetFileProvider
{
    /// <summary>
    /// Gets or sets the file path of the asset in the real file system.
    /// </summary>
    public string FilePath { get; set; } = filePath;

    /// <inheritdoc/>
    public Stream GetAssetStream()
    {
        return new FileStream(FilePath, FileMode.Open, FileAccess.Read);
    }
}
