namespace AtomicMassa.Editor.Studio.Panels.Inspector.Editors.Types;

/// <summary>
/// String property editor
/// </summary>
[CustomEditor(typeof(string))]
public class StringPropertyEditor : SimpleTextFieldPropertyEditor
{
    /// <summary>
    /// Simple text box
    /// </summary>
    protected TextBox TextBox { get; private set; }

    /// <inheritdoc/>
    public StringPropertyEditor(MemberInfo memberInfo, object targetObject)
        : base(memberInfo)
    {
        TextBox = new TextBox();
        Panel.Children.Add(TextBox);

        TextBox.Text = (string)(memberInfo.GetValue(targetObject) ?? "");
        _ = TextBox
            .GetObservable(TextBox.TextProperty)
            .Subscribe(text =>
            {
                memberInfo.SetValue(targetObject, text ?? string.Empty);
            });
    }
}
