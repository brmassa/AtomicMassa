namespace AtomicMassa.Editor.Studio.Panels.Inspector.Editors;

/// <summary>
/// Property editor for simple primitive types (int, string, etc)
/// </summary>
public abstract class SimpleTextFieldPropertyEditor : UserControl
{
    /// <summary>
    /// The panel that will contain the editor itself and the label
    /// </summary>
    protected StackPanel Panel { get; private set; }

    /// <summary>
    /// The label
    /// </summary>
    protected TextBlock Label { get; private set; }

    /// <summary>
    /// Create a new simple property editor
    /// </summary>
    protected SimpleTextFieldPropertyEditor(MemberInfo? memberInfo)
    {
        Panel = new StackPanel
        {
            Orientation = Orientation.Horizontal,
            Margin = new Thickness(0, 5, 0, 5)
        };

        Label = new TextBlock
        {
            Width = 100,
            Text = (memberInfo?.Name ?? "").ToReadableName()
        };
        Panel.Children.Add(Label);

        Content = Panel;
    }
}
