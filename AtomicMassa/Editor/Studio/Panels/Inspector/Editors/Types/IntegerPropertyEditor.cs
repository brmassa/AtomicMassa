namespace AtomicMassa.Editor.Studio.Panels.Inspector.Editors.Types;

/// <summary>
/// byte Byte property editor
/// </summary>
/// <inheritdoc/>
[CustomEditor(typeof(byte))]
public class BytePropertyEditor(MemberInfo memberInfo, object targetObject) : IntegerPropertyEditor<byte>(memberInfo, targetObject);

/// <summary>
/// sbyte SByte property editor
/// </summary>
/// <inheritdoc/>
[CustomEditor(editorType: typeof(sbyte))]
public class SbytePropertyEditor(MemberInfo memberInfo, object targetObject) : IntegerPropertyEditor<sbyte>(memberInfo, targetObject);

/// <summary>
/// short Int16 property editor
/// </summary>
/// <inheritdoc/>
[CustomEditor(typeof(short))]
public class ShortPropertyEditor(MemberInfo memberInfo, object targetObject) : IntegerPropertyEditor<short>(memberInfo, targetObject);

/// <summary>
/// ushort UInt16 property editor
/// </summary>
/// <inheritdoc/>
[CustomEditor(editorType: typeof(ushort))]
public class UshortPropertyEditor(MemberInfo memberInfo, object targetObject) : IntegerPropertyEditor<ushort>(memberInfo, targetObject);

/// <summary>
/// int Int32 property editor
/// </summary>
/// <inheritdoc/>
[CustomEditor(editorType: typeof(int))]
public class IntPropertyEditor(MemberInfo memberInfo, object targetObject) : IntegerPropertyEditor<int>(memberInfo, targetObject);

/// <summary>
/// uint UInt32 property editor
/// </summary>
/// <inheritdoc/>
[CustomEditor(editorType: typeof(uint))]
public class UintPropertyEditor(MemberInfo memberInfo, object targetObject) : IntegerPropertyEditor<uint>(memberInfo, targetObject);

/// <summary>
/// long Int64 property editor
/// </summary>
/// <inheritdoc/>
[CustomEditor(typeof(long))]
public class LongPropertyEditor(MemberInfo memberInfo, object targetObject) : IntegerPropertyEditor<long>(memberInfo, targetObject);

/// <summary>
/// ulong UInt64 property editor
/// </summary>
/// <inheritdoc/>
[CustomEditor(typeof(ulong))]
public class UlongPropertyEditor(MemberInfo memberInfo, object targetObject) : IntegerPropertyEditor<ulong>(memberInfo, targetObject);

/// <summary>
/// Generic property editor for integer types (int, uint, long, etc)
/// </summary>
public class IntegerPropertyEditor<T> : SimpleTextFieldPropertyEditor
    where T : IComparable, IConvertible
{
    private Control ValueControl { get; }

    /// <inheritdoc/>
    public IntegerPropertyEditor(MemberInfo memberInfo, object targetObject)
        : base(memberInfo)
    {
        INumberControlStrategy<T> controlStrategy;
        if (memberInfo.GetCustomAttribute<RangeAttribute>() is not null)
        {
            controlStrategy = new SliderStrategy<T>();
        }
        else if (memberInfo.GetCustomAttribute<NumericUpDownAttribute>() is not null)
        {
            controlStrategy = new NumericUpDownStrategy<T>();
        }
        else
        {
            controlStrategy = new TextBoxStrategy<T>();
        }

        ValueControl = controlStrategy.CreateControl(memberInfo, targetObject);

        Panel.Children.Add(ValueControl);
    }
}

/// <summary>
///
/// </summary>
/// <typeparam name="T"></typeparam>
public interface INumberControlStrategy<T>
    where T : IComparable, IConvertible
{
    /// <summary>
    /// Create a control for editing the value
    /// </summary>
    /// <param name="targetObject"></param>
    /// <param name="memberInfo"></param>
    /// <returns></returns>
    Control CreateControl(MemberInfo memberInfo, object targetObject);
}

/// <summary>
/// Create a slider control for editing the numeric value
/// </summary>
/// <typeparam name="T"></typeparam>
public class SliderStrategy<T> : INumberControlStrategy<T>
    where T : IComparable, IConvertible
{
    /// <inheritdoc/>
    public Control CreateControl(MemberInfo memberInfo, object targetObject)
    {
        var rangeAttr = memberInfo.GetCustomAttribute<RangeAttribute>()!;
        var control = new Slider
        {
            Minimum = Convert.ToDouble(rangeAttr.Min),
            Maximum = Convert.ToDouble(rangeAttr.Max),
            Value = Convert.ToDouble(
                (T)(memberInfo.GetValue(targetObject) ?? 0),
                CultureInfo.CurrentCulture
            )
        };
        _ = control
            .GetObservable(Avalonia.Controls.Primitives.RangeBase.ValueProperty)
            .Subscribe(val =>
            {
                memberInfo.SetValue(
                    targetObject,
                    (T)Convert.ChangeType(val, typeof(T), CultureInfo.CurrentCulture)
                );
            });
        return control;
    }
}

/// <summary>
/// Create a numeric up down control for editing the numeric value
/// </summary>
/// <typeparam name="T"></typeparam>
public class NumericUpDownStrategy<T> : INumberControlStrategy<T>
    where T : IComparable, IConvertible
{
    /// <inheritdoc/>
    public Control CreateControl(MemberInfo memberInfo, object targetObject)
    {
        var control = new NumericUpDown
        {
            Value = //(double)
                Convert.ToDecimal(
                    (T)(memberInfo.GetValue(targetObject) ?? 0),
                    CultureInfo.CurrentCulture
                )
        };
        _ = control
            .GetObservable(NumericUpDown.ValueProperty)
            .Subscribe(val =>
            {
                if (val is null)
                {
                    return;
                }

                memberInfo.SetValue(
                    targetObject,
                    Convert.ChangeType(val, typeof(T), CultureInfo.CurrentCulture)
                );
            });
        return control;
    }
}

/// <summary>
/// Create a text box control for editing the numeric value.
/// Despite being the simpliest control (visually), it requires the most code to handle
/// the convertion of text to the numeric value.
/// </summary>
/// <typeparam name="T"></typeparam>
public class TextBoxStrategy<T> : INumberControlStrategy<T>
    where T : IComparable, IConvertible
{
    /// <inheritdoc/>
    public Control CreateControl(MemberInfo memberInfo, object targetObject)
    {
        var control = new TextBox { Text = SetValue(memberInfo, targetObject).ToString() };

        _ = control
            .GetObservable(TextBox.TextProperty)
            .Subscribe(text =>
            {
                var sanitizeValue = SanitizeValue(text ?? string.Empty)!;
                control.Text = sanitizeValue.ToString();
                memberInfo.SetValue(targetObject, sanitizeValue);
            });
        return control;
    }

    /// <inheritdoc/>
    public T SetValue(MemberInfo memberInfo, object targetObject)
    {
        var input = memberInfo.GetValue(targetObject)?.ToString();

        if (string.IsNullOrEmpty(input))
        {
            // throw new ArgumentException("The provided string was empty or null.", nameof(memberInfo));
            input = "";
        }

        var sanitizeValue = SanitizeValue(input);

        return sanitizeValue;
    }

    private static T GetMinValue()
    {
        var maxProp = typeof(T).GetProperty("MinValue", BindingFlags.Public | BindingFlags.Static);
        if (maxProp != null)
        {
            return (T)maxProp.GetValue(null)!;
        }
        return default!;
    }

    private static T SanitizeValue(string input)
    {
        // Ignore non-digit characters
        // var sanitizedInput = new string(input.Where(char.IsDigit).ToArray());
        var sanitizedInput = input;

        // Parse the input string to the specified type
        T value;
        try
        {
            value = (T)Convert.ChangeType(sanitizedInput, typeof(T), CultureInfo.CurrentCulture);
        }
        catch
        {
            value = GetMinValue();
        }

        return value;
    }
}
