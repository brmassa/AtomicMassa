using AtomicMassa.Types;
using JeremyAnsel.Media.WavefrontObj;

namespace AtomicMassa.Engine.Core.Graphics;

/// <summary>
/// Represents a struct for building models from Wavefront OBJ files.
/// </summary>
public struct ModelBuilder
{
    /// <summary>
    /// Gets or sets the array of vertices for the model.
    /// </summary>
    public Vertex[] Vertices { get; set; }

    /// <summary>
    /// Gets or sets the array of indices for the model.
    /// </summary>
    public uint[] Indices { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="ModelBuilder"/> struct.
    /// </summary>
    public ModelBuilder()
    {
        Vertices = [];
        Indices = [];
    }

    /// <summary>
    /// Loads a 3D model from a Wavefront OBJ file and converts it into vertices and indices.
    /// </summary>
    /// <param name="path">The path to the Wavefront OBJ file to load.</param>
    public void LoadModel(string path)
    {
        var objFile = ObjFile.FromFile(path);

        var vertexMap = new Dictionary<Vertex, uint>();
        var vertices = new List<Vertex>();
        var indices = new List<uint>();

        foreach (var face in objFile.Faces)
        {
            foreach (var vFace in face.Vertices)
            {
                var vertexIndex = vFace.Vertex;
                var vertex = objFile.Vertices[vertexIndex - 1];
                var positionOut = new Vector3D<float>(
                    vertex.Position.X,
                    -vertex.Position.Y,
                    vertex.Position.Z
                );

                Vector3D<float> colorOut;
                if (vertex.Color is not null)
                {
                    colorOut = new(
                        vertex.Color.Value.X,
                        vertex.Color.Value.Y,
                        vertex.Color.Value.Z
                    );
                }
                else
                {
                    colorOut = new(1f, 1f, 1f);
                }

                var normalIndex = vFace.Normal;
                var normal = objFile.VertexNormals[normalIndex - 1];
                var normalOut = new Vector3D<float>(normal.X, -normal.Y, normal.Z);

                var textureIndex = vFace.Texture;
                var texture = objFile.TextureVertices[textureIndex - 1];

                // Flip Y for OBJ in Vulkan
                var textureOut = new Vector2D<float>(texture.X, -texture.Y);

                Vertex vertexOut =
                    new()
                    {
                        Position = positionOut,
                        Color = colorOut,
                        Normal = normalOut,
                        Uv = textureOut
                    };
                if (vertexMap.TryGetValue(vertexOut, out var meshIndex))
                {
                    indices.Add(meshIndex);
                }
                else
                {
                    indices.Add((uint)vertices.Count);
                    vertexMap[vertexOut] = (uint)vertices.Count;
                    vertices.Add(vertexOut);
                }
            }
        }

        Vertices = [.. vertices];
        Indices = [.. indices];

        Log.Logger.Lap(
            "Asset.obj",
            $"loaded {path}\t{Vertices.Length} verts,\t{Indices.Length} indices"
        );
    }
}
