using AtomicMassa.Engine.Core.Asset;

namespace AtomicMassa.Editor.AssetImporter;

/// <summary>
/// Import 3d model assets
/// </summary>
public class ModelAssetImporter : IAssetImporter
{
    #region IAssetImporter

    /// <inheritdoc/>
    public bool IsValid(string filePath)
    {
        if (string.IsNullOrEmpty(filePath))
        {
            return false;
        }

        return filePath.EndsWith(".obj", StringComparison.InvariantCultureIgnoreCase);
    }

    /// <inheritdoc/>
    public Asset CreateAsset(string filePath)
    {
        return new ModelAsset { RelativePath = filePath };
    }

    #endregion IAssetImporter
}
