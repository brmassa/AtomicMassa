using AtomicMassa.Engine.Core.Asset;

namespace AtomicMassa.Editor.AssetImporter;

/// <summary>
/// Generic testing importer
/// </summary>
[DefaultOption]
public class GenericAssetImporter : IAssetImporter
{
    #region IAssetImporter

    /// <inheritdoc/>
    public bool IsValid(string filePath)
    {
        return true;
    }

    /// <inheritdoc/>
    public Asset CreateAsset(string filePath)
    {
        return new Asset { RelativePath = filePath };
    }

    #endregion IAssetImporter
}
