
using AtomicMassa.Editor.Build.Settings;

namespace AtomicMassa.Tests.AtomicMassa.Editor.Build;

/// <summary>
/// Test CsProjectGenerator
/// </summary>
public class BuildAppSetttingsTest
{
    private readonly BuildAppSettings settings;

    /// <summary>
    /// Ctor
    /// </summary>
    public BuildAppSetttingsTest()
    {
        settings = new()
        {
            Title = "My App is: super cool",
            AssetJsonAbsolutePath = "/folder/app/project.app.json"
        };
    }

    /// <summary>
    /// Validate the paths generated
    /// </summary>
    [Fact]
    public void CheckPaths()
    {
        Assert.Equal(Path.GetDirectoryName("/folder/app/project.app.json"), settings.ProjectAbsoluteDir);
        Assert.Equal(Path.GetDirectoryName("/folder/app/Assets/"), settings.AssetsAbsoluteDir);
        Assert.Equal(Path.GetDirectoryName("/folder/app/.Cache/"), settings.CacheAbsoluteDir);
        Assert.Equal(Path.GetDirectoryName("../../Source/"), settings.CacheSourceRelativeDir);
    }

    /// <summary>
    /// Validate the convertion of the app Title to something valid for paths
    /// </summary>
    [Fact]
    public void CheckTitle()
    {
        Assert.Equal("My App is  super cool", settings.TitleToPathFriendly);
    }

    /// <summary>
    /// Check the lists of packages
    /// </summary>
    [Fact]
    public void PackagesExists()
    {
        Assert.NotEmpty(settings.AtomicMassaPackages);
        Assert.NotEmpty(settings.InternalPackages);
        Assert.NotEmpty(settings.Packages);
        Assert.NotEmpty(settings.PackageReferences);
        Assert.NotEmpty(settings.TargetFramework);
        Assert.NotEmpty(settings.TargetSdk);
    }
}
