namespace AtomicMassa.Editor.Studio.Panels.SceneTree;

/// <summary>
/// Represents a node in the scene hierarchy.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="SceneTreeNode"/> class.
/// </remarks>
/// <param name="node">The node associated with this tree node.</param>
[INotifyPropertyChanged]
public sealed partial class SceneTreeNode(Node node) : AmObject
{
    /// <summary>
    /// Gets or sets a value indicating whether this node is expanded.
    /// </summary>
    [ObservableProperty]
    private bool isExpanded = true;

    /// <summary>
    /// Gets the name of the node.
    /// </summary>
    public string Name => Node.Name;

    /// <summary>
    /// Gets a value indicating whether this node is active.
    /// </summary>
    public bool IsActive => Node.IsActive;

    /// <summary>
    /// Gets or sets the node associated with this tree node.
    /// </summary>
    public Node Node { get; set; } = node;

    /// <summary>
    /// Node parent.
    /// </summary>
    public SceneTreeNode? Parent { get; set; }

    /// <summary>
    /// Gets or sets the children of this node.
    /// </summary>
    public ObservableCollection<SceneTreeNode> Children { get; } = [];
}
