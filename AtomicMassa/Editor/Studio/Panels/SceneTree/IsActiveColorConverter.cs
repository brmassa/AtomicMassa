using Avalonia.Data.Converters;
using Avalonia.Media;

namespace AtomicMassa.Editor.Studio.Panels.SceneTree;

/// <summary>
/// Return the "disabled" color in case it IsActive == false
/// </summary>
public class IsActiveColorConverter : IValueConverter
{
    /// <inheritdoc />
    public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is bool isActive)
        {
            return isActive
                ? AvaloniaProperty.UnsetValue
                : Application.Current?.Resources["ThemeDisabledBrush"] ?? Brushes.Gray;
        }
        return AvaloniaProperty.UnsetValue;
    }

    /// <inheritdoc />
    public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotSupportedException();
    }
}
