using Avalonia.Media;

namespace AtomicMassa.Editor.Studio.Panels.Inspector.Editors;

internal static class VectorValueColors
{
    static public Color X => new(255, 255, 0, 0);
    static public Color Y => new(255, 0, 255, 0);
    static public Color Z => new(255, 0, 0, 255);
}