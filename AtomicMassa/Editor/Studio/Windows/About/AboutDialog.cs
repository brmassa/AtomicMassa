namespace AtomicMassa.Editor.Studio.Windows.About;

/// <summary>
/// A dialog window that shows information about the application.
/// </summary>
internal sealed partial class AboutDialog : Window
{
    [MenuItem("Help/About")]
    public static void ShowAbout(Window parent)
    {
        // Ensure UI operations are dispatched to the UI thread.
        Dispatcher.UIThread.Post(() =>
        {
            _ = new AboutDialog().ShowDialog(parent);
        });
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AboutDialog"/> class.
    /// </summary>
    public AboutDialog()
    {
        InitializeComponent();

        VersionLabel.Content = GetVersion();
        ContributorsLabel.Content = GetContributors();
        ReleaseDateLabel.Content = GetReleaseDate();
    }

    private static string GetVersion()
    {
        var assembly = Assembly.GetEntryAssembly();
        var version = assembly
            ?.GetCustomAttribute<AssemblyInformationalVersionAttribute>()
            ?.InformationalVersion;
        return $"{version}";
    }

    /// <summary>
    /// Gets the list of contributors. It's parsed from the CONTRIBUTORS.md file.
    /// </summary>
    private static string GetContributors()
    {
        return $"{Contributors.List}";
    }

    /// <summary>
    /// Gets the release date. It's parsed from the CONTRIBUTORS.md file.
    /// </summary>
    private static string GetReleaseDate()
    {
        // FIXME: compilation date was placed in the Contributors list code generation.
        // It should be placed in the ReleaseDate property.
        return $"{Contributors.CompilationDate}";
    }

    // Add this method to handle the Close button click event
    private void CloseButton_Click(object sender, RoutedEventArgs _)
    {
        Close();
    }
}
