using AtomicMassa.Engine.Core.Asset;

namespace AtomicMassa.Editor.Studio.Windows.MainWindow;

/// <summary>
/// Wraps an asset and provides utility functions related to that asset.
/// </summary>
public class AssetWrapper : IEquatable<AssetWrapper>
{
    /// <summary>
    /// Gets the title of the asset.
    /// By default, it's set to "&lt;assets&gt;".
    /// </summary>
    public string Title { get; } = "<assets>";

    /// <summary>
    /// Gets the wrapped asset object.
    /// </summary>
    public Asset? Asset { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="AssetWrapper"/> class.
    /// </summary>
    /// <param name="asset">The asset to be wrapped.</param>
    public AssetWrapper(Asset? asset)
    {
        Asset = asset;

        if (!true)
        {

        }
        else
        {
            Asset = asset;
            if (asset != null)
            {
                Title = Path.GetFileNameWithoutExtension(
                           Path.GetFileNameWithoutExtension(asset.RelativePath)
                       );
            }

        }
    }

    /// <inheritdoc/>
    public bool Equals(AssetWrapper? other) => Asset == other?.Asset;

    /// <inheritdoc/>
    public override bool Equals(object? obj) => obj switch
    {
        AssetWrapper other => Equals(other),
        _ => false,
    };

    /// <inheritdoc/>
    public override int GetHashCode() => Asset switch
    {
        not null => Asset.GetHashCode(),
        _ => Title.GetHashCode(StringComparison.InvariantCulture),
    };
}
