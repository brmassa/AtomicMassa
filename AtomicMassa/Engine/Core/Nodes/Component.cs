namespace AtomicMassa.Engine.Core.Nodes;

/// <summary>
/// Represents a base class for components that can be attached to nodes in the engine.
/// </summary>
public abstract class Component : AmObject
{
    /// <summary>
    /// Gets or sets a value indicating whether the component is active.
    /// </summary>
    public bool IsActive { get; set; } = true;

    /// <summary>
    /// Gets the node to which this component is attached.
    /// </summary>
    public Node Node { get; private set; } = null!;

    /// <summary>
    /// Sets up the component with the specified node.
    /// </summary>
    /// <param name="node">The node to which this component is attached.</param>
    public void Setup(Node node)
    {
        Node = node;
    }

    /// <summary>
    /// Called when the component is started, allowing for initialization.
    /// </summary>
    public virtual void OnStart() { }

    /// <summary>
    /// Called during each update cycle, providing an opportunity to perform per-frame logic.
    /// </summary>
    /// <param name="deltaTime">The time elapsed since the last update.</param>
    public virtual void OnUpdate(float deltaTime) { }
}
