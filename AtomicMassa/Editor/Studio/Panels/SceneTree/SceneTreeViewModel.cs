using AtomicMassa.Editor.Build.Settings;
using AtomicMassa.Engine.Core.Asset;
using ReactiveUI;
using System.Collections.Specialized;

namespace AtomicMassa.Editor.Studio.Panels.SceneTree;

/// <summary>
/// The view model of the content tree panel
/// </summary>
[InternalService]
internal sealed class SceneTreeViewModel
{
    public ObservableCollection<SceneTreeNode> Items { get; private set; } = [];
    public ObservableCollection<SceneTreeNode> SelectedItems { get; } = [];

    private readonly ISelectedObjectProvider<AmObject> selectedObjectProvider;
    private readonly ISelectedObjectProvider<Asset> selectedAssetProvider;
    private readonly IAppSettings appSettings;

    private Node? nodeMain;

    public SceneTreeViewModel(
        IAppSettings appSettings,
        ISelectedObjectProvider<AmObject> selectedObjectProvider,
        ISelectedObjectProvider<Asset> selectedAssetProvider)
    {
        this.selectedObjectProvider = selectedObjectProvider;
        this.selectedAssetProvider = selectedAssetProvider;
        this.appSettings = appSettings;

        _ = this.selectedAssetProvider.SelectedObjectChanged
            .ObserveOn(RxApp.MainThreadScheduler)
            .Subscribe(args =>
            {
                if (args.EventArgs.New is Prefab prefab)
                {
                    nodeMain = prefab.GetContent(this.appSettings.ProjectAbsoluteDir);
                }
                else
                {
                    nodeMain = null;
                }
                _ = Dispatcher.UIThread.InvokeAsync(() => LoadSceneStructure(nodeMain));
            });
    }

    /// <summary>
    /// Loads the folder structure of the given root folder into the Items collection.
    /// </summary>
    /// <param name="node"></param>
    private void LoadSceneStructure(Node? node)
    {
        Items.Clear();

        if (node is not null)
        {
            SceneTreeNode sceneNode = new(node);
            Items.Add(sceneNode);
            LoadFolderContentsRecursive(node, sceneNode.Children, sceneNode);
        }
        SelectedItems.CollectionChanged += SelectedItemsChanged;
    }

    private void SelectedItemsChanged(object? sender, NotifyCollectionChangedEventArgs e)
    {
        if (e.Action == NotifyCollectionChangedAction.Add)
        {
            if (e.NewItems != null && e.NewItems.Count > 0 && e.NewItems[0] is SceneTreeNode sceneTreeViewNode)
            {
                SetSelectedObject(sceneTreeViewNode.Node);
            }
        }
    }

    private static void LoadFolderContentsRecursive(
        Node node,
        ObservableCollection<SceneTreeNode> items,
        SceneTreeNode parent
    )
    {
        try
        {
            foreach (var child in node.Children)
            {
                // Ensure both Node.Parent and SceneTreeNode.Parent are set
                child.Parent = node;
                SceneTreeNode sceneNode = new(child) { Parent = parent };
                items.Add(sceneNode);
                LoadFolderContentsRecursive(child, sceneNode.Children, sceneNode);
            }
        }
        catch (Exception ex)
        {
            Log.Fatal("Error loading folder contents: {Message}", ex.Message);
        }
    }

    public void SetSelectedObject(Node node)
    {
        selectedObjectProvider.SetSelectedObject(node);
    }

    public IEnumerable<Type> GetClassesWithNodeContextMenuAttribute() =>
        BuildManager.Instance.LoadedAssemblies
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type => type.GetCustomAttributes<NodeContextMenuAttribute>(false).Any())
            .OrderBy(type => type.Name)
    ;
}
