using System.Collections;

namespace AtomicMassa.Editor.Studio.Panels.Inspector.Editors.Types;

/// <summary>
/// Property editor for Lists
/// </summary>
/// <typeparam name="T"></typeparam>
public class ListPropertyEditor<T> : CompositePropertyEditor
{
    /// <summary>
    /// Add new item into the list
    /// </summary>
    protected Button AddButton { get; private set; }

    /// <summary>
    /// The targetobject as IList
    /// </summary>
    protected IList TargetList { get; private set; }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="memberInfo"></param>
    /// <param name="targetObject"></param>
    /// <exception cref="ArgumentException"></exception>
    public ListPropertyEditor(MemberInfo memberInfo, object targetObject)
        : base(memberInfo)
    {
        if (memberInfo.GetValue(targetObject) is not IList targetList)
        {
            throw new ArgumentException("The member is not a list type");
        }

        TargetList = targetList;

        Content = Panel;

        AddButton = new Button { Content = "Add new item" };
        AddButton.Click += (sender, e) => AddNewItem(sender!, e);

        Panel.Children.Add(AddButton);

        foreach (var item in TargetList)
        {
            Panel.Children.Add(CreateListItemEditor(item));
        }
    }

    /// <summary>
    /// Add items.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="_"></param>
    protected virtual void AddNewItem(object sender, RoutedEventArgs _)
    {
        var newItem = Activator.CreateInstance<T>();
        if (newItem is null)
        {
            return;
        }


        TargetList.Add(newItem);

        Panel.Children.Insert(Panel.Children.Count - 1, CreateListItemEditor(newItem));
    }

    /// <summary>
    /// Add item.
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    protected UserControl CreateListItemEditor(object item)
    {
        // Delegate to another property editor here.
        // Note that this will require you to write or use existing PropertyEditor for the list item's type.
        // var itemEditor = new SomePropertyEditor(item);

        var itemEditor = new UserControl(); // replace this with the above line

        var deleteButton = new Button { Content = "X" };
        deleteButton.Click += (_, _) =>
        {
            Panel.Children.Remove(itemEditor);
            var itemIndex = TargetList.IndexOf(item);
            TargetList.RemoveAt(itemIndex);
        };

        var dragArea = new Border();
        dragArea.PointerPressed += (sender, e) =>
        {
            _ = DragDrop.DoDragDrop(e, new DataObject(), DragDropEffects.Move);
        };

        var itemPanel = new StackPanel { Orientation = Orientation.Horizontal };
        itemPanel.Children.Add(dragArea);
        itemPanel.Children.Add(itemEditor);
        itemPanel.Children.Add(deleteButton);

        var userControl = new UserControl { Content = itemPanel };

        return userControl;
    }
}
