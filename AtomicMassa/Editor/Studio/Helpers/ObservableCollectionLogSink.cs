using Serilog.Core;
using Serilog.Events;

namespace AtomicMassa.Editor.Studio.Helpers;

/// <summary>
/// Represents a logging sink that stores log entries in an ObservableCollection.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ObservableCollectionLogSink"/> class.
/// </remarks>
/// <param name="logs">The ObservableCollection to store log entries in.</param>
/// <param name="maxEntries">The maximum number of log entries to keep (default is 100).</param>
/// <exception cref="ArgumentNullException">Thrown if <paramref name="logs"/> is null.</exception>
public class ObservableCollectionLogSink(ObservableCollection<LogEvent> logs, int maxEntries = 100) : ILogEventSink
{
    private readonly ObservableCollection<LogEvent> logs = logs ?? throw new ArgumentNullException(nameof(logs));
    private readonly int maxEntries = maxEntries;

    /// <summary>
    /// Emits a log event by adding it to the ObservableCollection and trimming the collection
    /// if it exceeds the maximum number of entries.
    /// </summary>
    /// <param name="logEvent">The log event to emit.</param>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="logEvent"/> is null.</exception>
    public void Emit(LogEvent logEvent)
    {
        ArgumentNullException.ThrowIfNull(logs);

        logs.Add(logEvent);

        if (logs.Count > maxEntries)
        {
            logs.RemoveAt(0);
        }
    }
}
