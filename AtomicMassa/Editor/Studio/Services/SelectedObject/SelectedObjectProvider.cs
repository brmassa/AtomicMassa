namespace AtomicMassa.Editor.Studio.Services.SelectedObject;

/// <summary>
/// Provides a way to get and set the selected object.
/// </summary>
public class SelectedObjectProvider<T> : ISelectedObjectProvider<T>, IDisposable
{
    private readonly Subject<ObjectChangedEventArgs<T>> selectedObjectChangedSubject = new();

    /// <inheritdoc/>
    public T? SelectedObject { get; private set; }

    /// <inheritdoc/>
    public void SetSelectedObject(T? value)
    {
        var oldObject = SelectedObject;
        SelectedObject = value;
        selectedObjectChangedSubject.OnNext(
            new ObjectChangedEventArgs<T> { Old = oldObject, New = value }
        );
    }

    /// <inheritdoc/>
    public IObservable<EventPattern<ObjectChangedEventArgs<T>>> SelectedObjectChanged =>
        selectedObjectChangedSubject.Select(
            e => new EventPattern<ObjectChangedEventArgs<T>>(this, e)
        );

    /// <inheritdoc/>
    public void Dispose()
    {
        selectedObjectChangedSubject.Dispose();

        GC.SuppressFinalize(this);
    }
}
