using System.Diagnostics;
using System.Threading.Tasks;
using Serilog;

namespace AtomicMassa.Editor.Build;

/// <summary>
/// Export the final app
/// </summary>
/// <param name="settings"></param>
/// <param name="logger"></param>
public class ExportUserCode(IAppSettings settings, ILogger logger) : CompilerBase(settings, logger), IUserCodeCompiler
{
    /// <summary>
    /// Execute
    /// </summary>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public async Task<string> ExecuteAsync()
    {
        var csprojFilePath = await SetupAsync(GenerateExportCsProjFile);
        CreateOutputPathDirectories(csprojFilePath);


        // Dotnet Publish the project
        var exportOutputPath = ExportOutputDir(Settings.ProjectAbsoluteDir, "Output");
        Log.Information("Output path: {path}", exportOutputPath);
        CreateOutputPathDirectories(exportOutputPath);

        var startInfo = new ProcessStartInfo("dotnet", $"publish \"{csprojFilePath}\" -o \"{exportOutputPath}\" -c Release")
        {
            CreateNoWindow = true,
            UseShellExecute = false,
            RedirectStandardOutput = true,
            RedirectStandardError = true
        };

        using var process = new Process { StartInfo = startInfo };
        process.Start();
        var output = await process.StandardOutput.ReadToEndAsync();
        // var errors = await process.StandardError.ReadToEndAsync();
        process.WaitForExit();

        if (process.ExitCode != 0)
        {
            // Handle Export failures
            Logger.Error("Export failed with errors: {Errors}", output);
            throw new InvalidOperationException("Export failed.");
        }


        RenameExecutable(Settings, csprojFilePath, exportOutputPath);

        // Export succeeded
        Logger.Information("Export succeeded.");
        return exportOutputPath;
    }

    private string GenerateExportCsProjFile()
    {
        var project = CsProjectGenerator.GenerateExecutable(Settings, Logger);
        project.Save();
        return project.FullPath;
    }

    private static void RenameExecutable(BuildAppSettings settings, string csprojFilePath, string exportOutputPath)
    {
        var exportedExePath = Path.Combine(exportOutputPath, $"{Path.GetFileNameWithoutExtension(csprojFilePath)}.exe");
        var newExePath = Path.Combine(exportOutputPath, $"{settings.TitleToPathFriendly}{Path.GetExtension(exportedExePath)}");
        if (File.Exists(exportedExePath))
        {
            File.Move(exportedExePath, newExePath, true);
        }
    }

}
