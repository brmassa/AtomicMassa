namespace AtomicMassa.Types;

/// <summary>
/// Represents the state of a mouse button.
/// </summary>
public enum MouseButtonState
{
    /// <summary>
    /// The mouse button is in an undefined state.
    /// </summary>
    None,

    /// <summary>
    /// The mouse button is up (not pressed).
    /// </summary>
    Up,

    /// <summary>
    /// The mouse button is down (pressed).
    /// </summary>
    Down
}
