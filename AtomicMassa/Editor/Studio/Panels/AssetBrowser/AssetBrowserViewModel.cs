using System.Collections.Specialized;
using AtomicMassa.Editor.Build.Settings;
using AtomicMassa.Engine.Core.Asset;

namespace AtomicMassa.Editor.Studio.Panels.AssetBrowser;

/// <summary>
/// The view model of the Asset tree panel
/// </summary>
[InternalService]
internal sealed class AssetBrowserViewModel
{
    public ObservableCollection<AssetBrowserNode> Items { get; } = [];
    public ObservableCollection<AssetBrowserNode> SelectedItems { get; } = [];

    private readonly ISelectedObjectProvider<AmObject> selectedObjectProvider;
    private readonly ISelectedObjectProvider<Asset> selectedAssetProvider;

    public AssetBrowserViewModel(
        IAppSettings appSettings,
        ISelectedObjectProvider<AmObject> selectedObjectProvider,
        ISelectedObjectProvider<Asset> selectedAssetProvider)
    {
        this.selectedObjectProvider = selectedObjectProvider;
        this.selectedAssetProvider = selectedAssetProvider;

        LoadFolderStructure(appSettings.AssetsAbsoluteDir);
    }

    /// <summary>
    /// Loads the folder structure of the given root folder into the Items collection.
    /// </summary>
    /// <param name="rootFolderPath"></param>
    public void LoadFolderStructure(string rootFolderPath)
    {
        Items.Clear();

        DirectoryInfo rootDirectoryInfo = new(rootFolderPath);
        LoadFolderContentsRecursive(rootDirectoryInfo, Items);

        SelectedItems.CollectionChanged += SelectedNodes_CollectionChanged;
    }

    private void SelectedNodes_CollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
    {
        if (e.Action == NotifyCollectionChangedAction.Add)
        {
            if (e.NewItems != null && e.NewItems.Count > 0 && e.NewItems[0] is AssetBrowserNode newNode)
            {
                SetSelectedAsset(newNode);
            }
        }
    }

    private static void LoadFolderContentsRecursive(
        DirectoryInfo directoryInfo,
        ObservableCollection<AssetBrowserNode> targetCollection
    )
    {
        try
        {
            foreach (var subDirectoryInfo in directoryInfo.GetDirectories())
            {
                AssetBrowserNode node = new(subDirectoryInfo);
                targetCollection.Add(node);
                LoadFolderContentsRecursive(subDirectoryInfo, node.Subfolders);
            }

            foreach (var fileInfo in directoryInfo.GetFiles())
            {
                // Check if the file has a .meta.json extension before adding it.
                if (fileInfo.Name.EndsWith(".meta.json", StringComparison.OrdinalIgnoreCase))
                {
                    AssetBrowserNode fileNode = new(fileInfo);
                    targetCollection.Add(fileNode);
                }
            }
        }
        catch (Exception ex)
        {
            Log.Fatal("Error loading folder contents: {Message}", ex.Message);
        }
    }

    public void SetSelectedObject(AssetBrowserNode assetNode)
    {
        selectedObjectProvider.SetSelectedObject(assetNode.Asset);
    }

    public void SetSelectedAsset(AssetBrowserNode assetNode)
    {
        selectedAssetProvider.SetSelectedObject(assetNode.Asset);
    }
}
