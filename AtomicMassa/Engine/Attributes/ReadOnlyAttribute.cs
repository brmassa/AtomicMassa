using System;

namespace AtomicMassa;

/// <summary>
/// Make it read-only in the inspector
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
public sealed class ReadOnlyAttribute : Attribute { }
