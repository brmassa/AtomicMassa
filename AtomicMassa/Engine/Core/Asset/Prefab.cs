namespace AtomicMassa.Engine.Core.Asset;

/// <summary>
/// Prefab Node
/// </summary>
public class Prefab : Asset
{
    /// <summary>
    /// Retrieves the content associated with the specified asset path.
    /// </summary>
    /// <param name="projectPath">The path of the project where the asset is in.</param>
    /// <returns>An instance of <see cref="AmObject"/> if the asset content is found; otherwise, null.</returns>
    public Node? GetContent(string projectPath)
    {
        try
        {
            return Node.Load(Path.Combine(projectPath, RelativePath), null);
        }
        catch
        {
            return null;
        }
    }
}
