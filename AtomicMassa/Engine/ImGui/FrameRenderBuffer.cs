// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

namespace AtomicMassa.Engine.ImGui;

internal struct FrameRenderBuffer
{
    public DeviceMemory VertexBufferMemory;
    public DeviceMemory IndexBufferMemory;
    public ulong VertexBufferSize;
    public ulong IndexBufferSize;
    public Silk.NET.Vulkan.Buffer VertexBuffer;
    public Silk.NET.Vulkan.Buffer IndexBuffer;
};
