namespace AtomicMassa.Engine.Core.Asset.Database;

/// <summary>
/// Represents a database for managing assets associated with the application.
/// </summary>
[InternalService(InternalServiceLifetime.Singleton)]
public class AssetDatabase
{
    /// <summary>
    /// Assets indexed by their unique identifiers.
    /// </summary>
    public Dictionary<Guid, RealFileSystemProvider> Assets { get; private set; } = [];

    private static AssetDatabase? instance;

    /// <summary>
    /// Gets the singleton instance of the <see cref="AssetDatabase"/>.
    /// </summary>
    public static AssetDatabase Instance =>
        instance ?? throw new InvalidOperationException("AssetDatabase not initialized");

    /// <summary>
    /// Initializes a new instance of the <see cref="AssetDatabase"/> class.
    /// </summary>
    public AssetDatabase()
    {
        instance = instance == null ? this : throw new InvalidOperationException("AssetDatabase not initialized");
    }

    /// <summary>
    /// Builds the asset database by scanning and indexing asset files within the specified folder.
    /// </summary>
    /// <param name="assetFolderPath">The folder containing asset files.</param>
    /// <param name="recursive">Whether to search for assets recursively in subdirectories.</param>
    public void BuildDatabase(string assetFolderPath, bool recursive = true)
    {
        var searchOption = recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
        var metaFiles = Directory.GetFiles(assetFolderPath, "*.meta.json", searchOption);

        foreach (var metaFile in metaFiles)
        {
            var asset = AmObject.Load<Asset>(metaFile);
            if (asset != null)
            {
                Assets[asset.Id] = new RealFileSystemProvider(asset.RelativePath);
            }
        }
    }

    /// <summary>
    /// Saves the asset database to a JSON file at the specified path.
    /// </summary>
    /// <param name="path">The path to save the database to.</param>
    public void SaveDatabase(string path)
    {
        var jsonString = JsonSerializer.Serialize(Assets);
        File.WriteAllText(path, jsonString);
    }

    /// <summary>
    /// Loads the asset database from a JSON file at the specified path.
    /// </summary>
    /// <param name="path">The path to load the database from.</param>P
    public void LoadDatabase(string path)
    {
        if (!File.Exists(path))
        {
            return;
        }

        var jsonString = File.ReadAllText(path);
        Assets =
            JsonSerializer.Deserialize<Dictionary<Guid, RealFileSystemProvider>>(jsonString)
            ?? [];
    }

    /// <summary>
    /// Merges the provided dictionary of assets into the asset database.
    /// </summary>
    /// <param name="assetsNew">The dictionary of assets to merge.</param>
    public void MergeDatabase(Dictionary<Guid, RealFileSystemProvider>? assetsNew)
    {
        if (assetsNew is null)
        {
            return;
        }

        foreach (var kvp in assetsNew)
        {
            Assets[kvp.Key] = kvp.Value;
        }
    }

    /// <summary>
    /// Initializes a file system watcher to monitor changes to asset files in the specified folder.
    /// </summary>
    /// <param name="assetFolderPath">The folder to monitor for changes.</param>
    public void InitializeWatcher(string assetFolderPath)
    {
        using FileSystemWatcher watcher =
            new()
            {
                Path = assetFolderPath,
                NotifyFilter =
                    NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName,
                Filter = "*.meta.json"
            };

        watcher.Changed += OnChanged;
        watcher.Created += OnChanged;
        watcher.Deleted += OnDeleted;

        watcher.EnableRaisingEvents = true;
    }

    private void OnChanged(object sender, FileSystemEventArgs e) { }

    private void OnDeleted(object sender, FileSystemEventArgs e) { }
}
