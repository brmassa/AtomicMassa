using Avalonia.Data.Converters;
using Material.Icons;
using Serilog.Events;

namespace AtomicMassa.Editor.Studio.Panels.Output;

/// <summary>
/// Given a log event level, return an icon
/// </summary>
public class LogEventLevelToIconConverter : IValueConverter
{
    /// <inheritdoc/>
    public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is not LogEventLevel logLevel)
        {
            return MaterialIconKind.Information;
        }

        return logLevel switch
        {
            LogEventLevel.Information or LogEventLevel.Debug => MaterialIconKind.Information,
            LogEventLevel.Warning => MaterialIconKind.Warning,
            LogEventLevel.Error or LogEventLevel.Fatal => MaterialIconKind.AlertDecagram,
            _ => MaterialIconKind.Information,
        };
    }

    /// <inheritdoc/>
    public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}
