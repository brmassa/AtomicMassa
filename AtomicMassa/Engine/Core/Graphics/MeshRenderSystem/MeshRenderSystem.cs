using AtomicMassa.Engine.Core.Graphics.StandardRenderSystem;

namespace AtomicMassa.Engine.Core.Graphics.MeshRenderSystem;

/// <summary>
/// Mesh Renderer can draw custom created solids
/// </summary>
public class MeshRenderSystem : IRenderSystem, IDisposable
{
    private readonly Vulkan vulkan;

    private const string taskShaderPath = "testMesh.task.spv";
    private const string meshShaderPath = "testMesh.mesh.spv";
    private const string fragShaderPath = "testMesh.frag.spv";
    private const string rendererName = "Mesh2Renderer";

    private MeshPipeline pipeline = null!;
    private PipelineLayout pipelineLayout;

    /// <summary>
    /// .ctor
    /// </summary>
    /// <param name="vulkan"></param>
    /// <param name="renderPass"></param>
    /// <param name="globalSetLayout"></param>
    public MeshRenderSystem(
        Vulkan vulkan,
        RenderPass renderPass,
        Silk.NET.Vulkan.DescriptorSetLayout globalSetLayout
    )
    {
        this.vulkan = vulkan;
        CreatePipelineLayout(globalSetLayout);
        CreatePipeline(renderPass);
    }

    /// <inheritdoc />
    public unsafe void Render(FrameInfo frameInfo, ref GlobalUbo ubo)
    {
        pipeline.Bind(frameInfo.CommandBuffer);

        vulkan.Vk.CmdBindDescriptorSets(
            frameInfo.CommandBuffer,
            PipelineBindPoint.Graphics,
            pipelineLayout,
            0,
            1,
            frameInfo.GlobalDescriptorSet,
            0,
            null
        );

        foreach (var go in Node.GetTypeAndChildren<MeshNode>(frameInfo.Nodes))
        {
            StandardPushConstantData push = new() { ModelMatrix = go.TransformationMatrix(), };
            vulkan.Vk.CmdPushConstants(
                frameInfo.CommandBuffer,
                pipelineLayout,
                ShaderStageFlags.FragmentBit
                    | ShaderStageFlags.MeshBitNV
                    | ShaderStageFlags.TaskBitNV,
                0,
                StandardPushConstantData.SizeOf(),
                ref push
            );
            go.Draw(frameInfo.CommandBuffer);
        }
    }

    /// <inheritdoc />
    public unsafe void Dispose()
    {
        pipeline.Dispose();
        vulkan.Vk.DestroyPipelineLayout(vulkan.Device.VkDevice, pipelineLayout, null);
        GC.SuppressFinalize(this);
    }

    private unsafe void CreatePipelineLayout(Silk.NET.Vulkan.DescriptorSetLayout globalSetLayout)
    {
        var descriptorSetLayouts = new Silk.NET.Vulkan.DescriptorSetLayout[] { globalSetLayout };
        PushConstantRange pushConstantRange =
            new()
            {
                StageFlags =
                    ShaderStageFlags.FragmentBit
                    | ShaderStageFlags.MeshBitNV
                    | ShaderStageFlags.TaskBitNV,
                Offset = 0,
                Size = StandardPushConstantData.SizeOf(),
            };

        fixed (Silk.NET.Vulkan.DescriptorSetLayout* descriptorSetLayoutPtr = descriptorSetLayouts)
        {
            PipelineLayoutCreateInfo pipelineLayoutInfo =
                new()
                {
                    SType = StructureType.PipelineLayoutCreateInfo,
                    SetLayoutCount = (uint)descriptorSetLayouts.Length,
                    PSetLayouts = descriptorSetLayoutPtr,
                    PushConstantRangeCount = 1,
                    PPushConstantRanges = &pushConstantRange,
                };

            var resultVulkan = vulkan.Vk.CreatePipelineLayout(vulkan.Device.VkDevice, pipelineLayoutInfo, null, out pipelineLayout);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to create pipeline layout: {0}", resultVulkan);
            }
        }
    }

    private void CreatePipeline(RenderPass renderPass)
    {
        Debug.Assert(pipelineLayout.Handle != 0, "Cannot create pipeline before pipeline layout");

        var pipelineConfig = new PipelineConfigInfo();
        MeshPipeline.DefaultPipelineConfigInfo(ref pipelineConfig);
        MeshPipeline.EnableMultiSampling(ref pipelineConfig, vulkan.Device.MsaaSamples);

        var inputAssemblyInfo = pipelineConfig.InputAssemblyInfo;
        inputAssemblyInfo.Topology = PrimitiveTopology.TriangleStrip;
        pipelineConfig.InputAssemblyInfo = inputAssemblyInfo;

        pipelineConfig.RenderPass = renderPass;
        pipelineConfig.PipelineLayout = pipelineLayout;
        pipeline = new MeshPipeline(
            vulkan.Vk,
            vulkan.Device,
            taskShaderPath,
            meshShaderPath,
            fragShaderPath,
            pipelineConfig,
            rendererName
        );
    }
}
