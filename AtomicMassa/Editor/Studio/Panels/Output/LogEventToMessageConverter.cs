using Avalonia.Data.Converters;
using Serilog.Events;

namespace AtomicMassa.Editor.Studio.Panels.Output;

/// <summary>
/// Converts a <see cref="LogEvent"/> to its rendered message.
/// </summary>
public class LogEventToMessageConverter : IValueConverter
{
    /// <inheritdoc/>
    public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
        => value is LogEvent logEvent ?
            LogEntryFull(logEvent, culture) :
            (object)string.Empty;

    /// <inheritdoc/>
    public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
        => throw new NotImplementedException();

    /// <summary>
    /// Generate a 2 line summary of the log event
    /// </summary>
    /// <param name="logEvent"></param>
    /// <param name="culture"></param>
    /// <returns></returns>
    public static string LogEntryFull(LogEvent? logEvent, CultureInfo culture)
    {
        if (logEvent is null)
        {
            return string.Empty;
        }

        var message = logEvent.RenderMessage(culture);
        if (message.Contains('\n', StringComparison.InvariantCulture))
        {
            // Split the message into lines and take the first two lines
            var lines = message.Split('\n');
            message = string.Join("\n", lines.Take(2));
        }

        return $"{logEvent.Timestamp.ToString(culture)}\n{message}";
    }
}
