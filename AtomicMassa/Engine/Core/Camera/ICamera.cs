namespace AtomicMassa.Engine.Core.Camera;

/// <summary>
/// Represents an interface for a camera that defines the properties and methods
/// required for controlling and manipulating the camera's view in a 3D scene.
/// </summary>
public interface ICamera
{
    /// <summary>
    /// Gets the aspect ratio of the camera's view.
    /// </summary>
    float AspectRatio { get; }

    /// <summary>
    /// Gets or sets the position of the camera in 3D space.
    /// </summary>
    Vector3D<float> Position { get; set; }

    /// <summary>
    /// Gets or sets the field of view angle of the camera's projection.
    /// </summary>
    float FieldOfView { get; set; }

    /// <summary>
    /// Gets the front vector of the camera.
    /// </summary>
    Vector3D<float> Front { get; }

    /// <summary>
    /// Gets the right vector of the camera.
    /// </summary>
    Vector3D<float> Right { get; }

    /// <summary>
    /// Gets the up vector of the camera.
    /// </summary>
    Vector3D<float> Up { get; }

    /// <summary>
    /// Gets the front vector of the camera as a 4D vector.
    /// </summary>
    Vector4D<float> FrontVector4 { get; }

    /// <summary>
    /// Gets the inverse view matrix of the camera.
    /// </summary>
    /// <returns>The inverse view matrix.</returns>
    Matrix4X4<float> GetInverseViewMatrix();

    /// <summary>
    /// Gets the projection matrix of the camera.
    /// </summary>
    /// <returns>The projection matrix.</returns>
    Matrix4X4<float> GetProjectionMatrix();

    /// <summary>
    /// Gets the view matrix of the camera.
    /// </summary>
    /// <returns>The view matrix.</returns>
    Matrix4X4<float> GetViewMatrix();

    /// <summary>
    /// Projects a 3D point in world space to a 2D point in screen space.
    /// </summary>
    /// <param name="mouse3d">The 3D point to project.</param>
    /// <returns>The projected 2D point in screen space.</returns>
    Vector2D<float> Project(Vector3D<float> mouse3d);

    /// <summary>
    /// Unprojects a 2D point in screen space to a 3D point in world space.
    /// </summary>
    /// <param name="mouse2d">The 2D point to unproject.</param>
    /// <returns>The unprojected 3D point in world space.</returns>
    Vector3D<float> UnProject(Vector2D<float> mouse2d);
}
