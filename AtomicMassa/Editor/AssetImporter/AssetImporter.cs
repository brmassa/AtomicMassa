using AtomicMassa.Editor.Build;

namespace AtomicMassa.Editor.AssetImporter;

/// <summary>
/// Scan and import into a database all the assets from "Assets" folder
/// </summary>
/// <remarks>
/// Constructor
/// </remarks>
public class AssetImporter(ILogger logger)
{
    private readonly List<IAssetImporter> assetCreators = BuildManager.Instance.LoadedAssemblies
            .SelectMany(s => s.GetTypes())
            .Where(p => typeof(IAssetImporter).IsAssignableFrom(p) && !p.IsInterface)
            .Select(
                t =>
                    new
                    {
                        Type = t,
                        IsLastResort = t.GetCustomAttributes(
                            typeof(DefaultOptionAttribute),
                            false
                        ).Length > 0
                    }
            )
            .OrderBy(x => x.IsLastResort) // Sort so that last resort is at the end
            .Select(x => Activator.CreateInstance(x.Type) as IAssetImporter)
            .Where(t => t != null)
            .ToList()!;
    private readonly ILogger logger = logger;

    /// <summary>
    /// Generate the meta file for each asset
    /// </summary>
    /// <param name="assetFolderPath"></param>
    public void GenerateMetaFiles(string assetFolderPath)
    {
        // Verify if the asset folder exists
        if (!Directory.Exists(assetFolderPath))
        {
            logger.Warning("The specified asset folder does not exist.");
            return;
        }

        // Recursively scan the asset folder
        ScanFolder(assetFolderPath);
    }

    private void ScanFolder(string folderPath)
    {
        // Get all files in the current folder
        var files = Directory.GetFiles(folderPath);

        // Iterate through each file and generate a meta file if it doesn't already exist
        foreach (var filePath in files)
        {
            GenerateMetaFile(filePath);
        }

        // Get all subfolders
        var subfolders = Directory.GetDirectories(folderPath);

        // Recursively scan each subfolder
        foreach (var subfolder in subfolders)
        {
            ScanFolder(subfolder);
        }
    }

    private void GenerateMetaFile(string filePath)
    {
        // Skip files that are already meta files
        if (filePath.EndsWith(".meta.json", StringComparison.InvariantCultureIgnoreCase))
        {
            return;
        }

        // Check if meta file already exists
        var metaFilePath = $"{filePath}.meta.json";
        if (File.Exists(metaFilePath))
        {
            return;
        }

        // Find a valid asset creator
        foreach (var creator in assetCreators)
        {
            if (creator.IsValid(filePath))
            {
                // Create asset and serialize it
                var assetData = creator.CreateAsset(filePath);
                var jsonString = JsonSerializer.Serialize(
                    assetData,
                    assetData.GetType(),
                    AmObject.JsonOptions
                );
                File.WriteAllText(metaFilePath, jsonString);
                break;
            }
        }
    }
}
