using System.Reflection;
using System.Runtime.Loader;
using System.Threading.Tasks;
using Microsoft.Build.Locator;
using Serilog;

namespace AtomicMassa.Editor.Build;

/// <summary>
/// Build and Load the user app
/// </summary>
public class BuildManager
{
    private readonly BuildAppSettings settings;
    private readonly ILogger logger;
    private AssemblyLoadContext? appContext;

    /// <summary>
    /// Compilation Sucess event
    /// </summary>
    public event Action<string>? OnCompilationSuccess;

    /// <summary>
    /// Compilation Fail event
    /// </summary>
    public event Action<string>? OnCompilationFailure;

    /// <summary>
    /// Compilation Sucess event
    /// </summary>
    public event Action<string>? OnBuildSuccess;

    /// <summary>
    /// Compilation Fail event
    /// </summary>
    public event Action<string>? OnBuildFailure;

    private static BuildManager? instance;

    /// <summary>
    /// Singleton
    /// </summary>
    public static BuildManager Instance =>
        instance ?? throw new InvalidOperationException("BuildManager not initialized");

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="settings"></param>
    /// <param name="logger"></param>
    /// <exception cref="InvalidOperationException"></exception>
    public BuildManager(IAppSettings settings, ILogger logger)
    {
        this.settings = new BuildAppSettings().Load(settings) as BuildAppSettings ?? throw new InvalidOperationException("Settings could not be loaded.");
        this.logger = logger;
        this.appContext = CreateAppContext;
        instance = instance == null ? this : throw new InvalidOperationException("BuildManager not initialized");
    }

    /// <summary>
    /// Just wrap the MSBuildLocator.RegisterDefaults.
    /// </summary>
    public static void MsBuildLocatorRegisterDefaults()
    {
        MSBuildLocator.RegisterDefaults();
    }

    /// <summary>
    /// List of all loaded Assemblies (editor or user app)
    /// </summary>
    public IEnumerable<Assembly> LoadedAssemblies =>
        // new[] { Assembly.GetEntryAssembly()! }
        // .Concat(appContext?.Assemblies ?? []);
        AppDomain.CurrentDomain.GetAssemblies();

    /// <summary>
    /// Load app assemblies
    /// </summary>
    public void LoadAssembly(string assemblyOutputPath)
    {
        try
        {
            logger.Information("Loading assembly from {AssemblyOutputPath}", assemblyOutputPath);
            appContext?.LoadFromAssemblyPath(assemblyOutputPath);
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Failed to load assembly from {AssemblyOutputPath}", assemblyOutputPath);
            throw;
        }
    }

    /// <summary>
    /// Unload app assemblies
    /// </summary>
    private void UnloadAssembly()
    {
        try
        {
            logger.Information("Unloading user assembly");
            appContext?.Unload();
            appContext = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            // Create a new AssemblyLoadContext for future assembly loads
            appContext = CreateAppContext;
            logger.Information("Assembly unloaded");
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Failed to unload user assembly");
            throw;
        }
    }

    // TODO: use recreateCsProj to force creating new csproj files or not
    private async Task<string> Compile(bool recreateCsProj)
    {
        try
        {
            logger.Information("Compiling user code");
            var userCodeCompiler = new CompileUserCode(settings, logger);
            var assemblyOutputPath = await userCodeCompiler.ExecuteAsync();
            return assemblyOutputPath;
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Compilation failed");
            throw;
        }
    }

    /// <summary>
    /// Reload app assemblies
    /// </summary>
    public void ReloadAssembly(string assemblyOutputPath)
    {
        UnloadAssembly();
        LoadAssembly(assemblyOutputPath);
        OnCompilationSuccess?.Invoke("Compilation and loading succeeded.");
    }

    /// <summary>
    /// Load the newly created DLL
    /// </summary>
    /// <param name="recreateCsProj"></param>
    /// <returns></returns>
    public async Task CompileAndLoadAssembly(bool recreateCsProj = false)
    {
        try
        {
            UnloadAssembly();
            var assemblyOutputPath = await Compile(recreateCsProj);
            LoadAssembly(assemblyOutputPath);
            OnCompilationSuccess?.Invoke("Compilation and loading succeeded.");
        }
        catch (Exception ex)
        {
            OnCompilationFailure?.Invoke("Compilation and loading failed: " + ex.Message);
        }
    }

    /// <summary>
    /// Export the app
    /// </summary>
    /// <returns></returns>
    public async Task Export()
    {
        try
        {
            var userCodeCompiler = new ExportUserCode(settings, logger);
            await userCodeCompiler.ExecuteAsync();
            OnBuildSuccess?.Invoke("Export sucessful.");
        }
        catch (Exception ex)
        {
            OnBuildFailure?.Invoke("Export failed: " + ex.Message);
        }
    }

    private static AssemblyLoadContext CreateAppContext => new("UserAssemblyLoadContext", isCollectible: true);
}
