namespace AtomicMassa.Engine.Core.Utils;

/// <summary>
/// Provides functionality for lap logging with various timing details.
/// </summary>
public static class LapLogger
{
    private static long firstTick;

    private static long lastTick;

    private static readonly Dictionary<string, long> laps = [];

    private const int dt = 12;

    private const int nt = -12;

    /// <summary>
    /// Restart the timer and initialize the logger information.
    /// </summary>
    /// <param name="logger">The logger instance.</param>
    public static void RestartTimer(this ILogger logger)
    {
        ArgumentNullException.ThrowIfNull(logger);

        firstTick = DateTime.Now.Ticks;
        lastTick = firstTick;
        logger.Information(
            $" {"Memory",8} | {"Total",dt + 2} | {"Last",dt} | {"Lap",dt} | {"Name",nt} | Message"
        );
        logger.Information(
            "--------------------------------------------------------------------------------------"
        );
    }

    /// <summary>
    /// Start a new lap with the specified name.
    /// </summary>
    /// <param name="lap">The name of the lap.</param>
    public static void StartLap(string lap) => laps[lap] = DateTime.Now.Ticks;

    /// <summary>
    /// Log the lap information.
    /// </summary>
    /// <param name="logger">The logger instance.</param>
    /// <param name="lap">The name of the lap.</param>
    /// <param name="msg">The message to log.</param>
    /// <param name="skipTimer">Whether to skip the timer or not.</param>
    public static void Lap(this ILogger? logger, string lap, string msg, bool skipTimer = false)
    {
        if (logger is null)
        {
            return;
        }

        var memory = Process.GetCurrentProcess().WorkingSet64 / 1_000_000;
        var now = DateTime.Now.Ticks;
        if (!skipTimer)
        {
            if (!laps.TryGetValue(lap, out var value))
            {
                value = now;
                laps[lap] = value;
            }
            var lapTicks = T2Ms(now - value);
            logger
                .ForContext("Lap", lap)
                .Information(
                    "{Memory,6} MB | {TotalElapsed,14} | {LastElapsed,12} | {LapTicks,12} | {Lap,12} | {Message}",
                    memory,
                    T2Ms(now - firstTick),
                    T2Ms(now - lastTick),
                    lapTicks,
                    lap,
                    msg
                );
            lastTick = now;
        }
        else
        {
            logger
                .ForContext("Lap", lap)
                .Information(
                    "{Memory,6} MB | {Empty,14} | {Empty,12} | {Empty,12} | {Empty,12} | {Message}",
                    memory,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    msg
                );
        }
    }

    /// <summary>
    /// Convert ticks to milliseconds string.
    /// </summary>
    /// <param name="tick">The tick value to convert.</param>
    /// <returns>The milliseconds representation in string.</returns>
    private static string T2Ms(long tick)
    {
        return $"{tick / 10_000d:#,##0.000}ms";
    }
}
