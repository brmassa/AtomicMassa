using AtomicMassa.Editor.Studio.Windows.MainWindow;
using AtomicMassa.Engine.Core.Asset;
using AtomicMassa.Engine.Core.Asset.Database;
using Serilog.Events;

namespace AtomicMassa.Editor.Studio;

internal sealed class App : Application
{
    public static new App Current => (App)Application.Current!;
    public IServiceProvider Services { get; }

    // This collection will store your logs
    public static ObservableCollection<LogEvent> LogCollection { get; } = [];

    public App()
    {
        Services = ConfigureServices();

        // Set up Serilog
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .Enrich.FromLogContext()
            .WriteTo.Console(formatProvider: CultureInfo.CurrentCulture)
            .WriteTo.Sink(new ObservableCollectionLogSink(LogCollection))
            .CreateLogger();

        Services = ConfigureServices();
    }

    public override void Initialize()
    {
        AvaloniaXamlLoader.Load(this);
    }

    public override void OnFrameworkInitializationCompleted()
    {
        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktopLifetime)
        {
            desktopLifetime.MainWindow = new MainWindow();
        }

        base.OnFrameworkInitializationCompleted();
    }

    private static ServiceProvider ConfigureServices()
    {
        var services = new ServiceCollection();

        services.AddSingleton(Log.Logger);
        services.AddSingleton<BuildManager>();
        services.AddSingleton<AssetDatabase>();
        services.AddSingleton<ISelectedObjectProvider<AmObject>, SelectedObjectProvider<AmObject>>();
        services.AddSingleton<ISelectedObjectProvider<Asset>, SelectedObjectProvider<Asset>>();
        services.AddSingleton<ISelectedObjectProvider<Node>, SelectedObjectProvider<Node>>();

        // Dynamically register services
        foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach (var type in assembly.GetTypes())
            {
                var attribute = type.GetCustomAttribute<InternalServiceAttribute>();
                if (attribute == null) { continue; }

                var serviceType = attribute.ServiceType ?? type;
                if (attribute.Lifetime == InternalServiceLifetime.Transient)
                {
                    _ = services.AddTransient(serviceType, type);
                }
                else if (attribute.Lifetime == InternalServiceLifetime.Singleton)
                {
                    _ = services.AddSingleton(serviceType, type);
                }
            }
        }

        return services.BuildServiceProvider();
    }
}
