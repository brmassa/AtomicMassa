namespace AtomicMassa.Engine.Core.Nodes;

/// <summary>
/// Represents a node in the engine that represents a light source.
/// </summary>
[NodeContextMenu]
public class LightNode : Node
{
    /// <summary>
    /// The intensity of the light.
    /// </summary>
    public float Intensity { get; set; } = 1;

    /// <summary>
    /// The color of the light as a 4D vector (RGBA).
    /// </summary>
    public Vector4D<float> Color { get; set; } = new(1.0f);

    /// <summary>
    /// Creates a new point light with the specified intensity and color.
    /// </summary>
    /// <param name="intensity">The intensity of the point light.</param>
    /// <param name="color">The color of the point light as a 4D vector (RGBA).</param>
    /// <returns>A new <see cref="LightNode"/> representing a point light source.</returns>
    public static LightNode CreatePointLight(float intensity, Vector4D<float> color)
    {
        // Create a new LightNode with the specified color and intensity.
        var lightNode = new LightNode { Color = color, Intensity = intensity };
        return lightNode;
    }
}
