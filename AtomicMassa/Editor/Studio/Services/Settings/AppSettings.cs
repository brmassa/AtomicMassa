using AtomicMassa.Editor.Build.Settings;

namespace AtomicMassa.Editor.Studio.Services.Settings;

/// <inheritdoc cref="IAppSettings"/>
[InternalService(InternalServiceLifetime.Singleton, typeof(IAppSettings))]
public class AppSettings : AmObject, IAppSettings
{
    /// <inheritdoc/>
    [JsonIgnore]
    public string? AssetJsonAbsolutePath { get; set; }

    /// <inheritdoc/>
    public string ProjectAbsoluteDir => Path.GetDirectoryName(AssetJsonAbsolutePath)!;

    /// <inheritdoc/>
    public string AssetsAbsoluteDir => Path.Combine(Path.GetFullPath(ProjectAbsoluteDir), "Assets");

    /// <inheritdoc/>
    public string? Title { get; set; }

    /// <inheritdoc/>
    public string? TitleToPathFriendly
    {
        get
        {
            if (string.IsNullOrEmpty(Title))
            {
                return string.Empty;
            }
            var invalidChars = Path.GetInvalidFileNameChars().Union(Path.GetInvalidPathChars()).ToArray();
            return new string(Title.Select(ch => invalidChars.Contains(ch) ? '_' : ch).ToArray());
        }
    }

    /// <inheritdoc/>
    public IAppSettings Load(IAppSettings appSettings)
    {
        ArgumentNullException.ThrowIfNull(appSettings);

        AssetJsonAbsolutePath = appSettings.AssetsAbsoluteDir;
        Title = appSettings.Title;
        return this;
    }
}
