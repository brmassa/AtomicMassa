using Avalonia.Media;
using Silk.NET.Maths;

namespace AtomicMassa.Editor.Studio.Panels.Inspector.Editors.Types;

/// <summary>
/// Custom editor for Vector3 properties.
/// </summary>
[CustomEditor(typeof(Vector3D<float>))]
public class Vector3DPropertyEditor : UserControl
{
    private readonly TextBox xBox;
    private readonly TextBox yBox;
    private readonly TextBox zBox;
    private readonly MemberInfo memberInfo;

    /// <summary>
    /// Content of the panel
    /// </summary>
    protected StackPanel Panel { get; init; } = new();

    /// <inheritdoc/>
    public Vector3DPropertyEditor(MemberInfo memberInfo, object targetObject)
    {
        this.memberInfo = memberInfo;

        // Horizontal StackPanel for dimension TextBoxes and Labels
        var dimensionsPanel = new StackPanel { Orientation = Orientation.Horizontal, Spacing = 5, };

        // X Dimension
        xBox = CreateDimensionTextBox(
            ((Vector3D<float>?)memberInfo.GetValue(targetObject))?.X ?? 0,
            VectorValueColors.Y,
            "X"
        );
        xBox.KeyUp += (sender, _) => UpdateVectorComponent(sender, targetObject);

        // Y Dimension
        yBox = CreateDimensionTextBox(
            ((Vector3D<float>?)memberInfo.GetValue(targetObject))?.Y ?? 0,
            VectorValueColors.Y,
            "Y"
        );
        yBox.KeyUp += (sender, _) => UpdateVectorComponent(sender, targetObject);

        // Z Dimension
        zBox = CreateDimensionTextBox(
            ((Vector3D<float>?)memberInfo.GetValue(targetObject))?.Z ?? 0,
            VectorValueColors.Z,
            "Z"
        );
        zBox.KeyUp += (sender, _) => UpdateVectorComponent(sender, targetObject);

        dimensionsPanel.Children.Add(CreateDimensionLabel("x", VectorValueColors.X));
        dimensionsPanel.Children.Add(xBox);
        dimensionsPanel.Children.Add(CreateDimensionLabel("y", VectorValueColors.Y));
        dimensionsPanel.Children.Add(yBox);
        dimensionsPanel.Children.Add(CreateDimensionLabel("z", VectorValueColors.Z));
        dimensionsPanel.Children.Add(zBox);

        var fieldPanel = new StackPanel
        {
            Orientation = Orientation.Horizontal,
            Margin = new Thickness(0, 5, 0, 5)
        };
        fieldPanel.Children.Add(
            new TextBlock
            {
                Text = (memberInfo?.Name ?? "").ToReadableName(),
                Width = 100
            }
        );
        fieldPanel.Children.Add(dimensionsPanel);
        Panel = new StackPanel
        {
            Orientation = Orientation.Vertical,
            Margin = new Thickness(0, 5, 0, 5)
        };
        Panel.Children.Add(fieldPanel);
        Content = Panel;
    }

    private static TextBox CreateDimensionTextBox(float value, Color color, string tooltip)
    {
        var textBox = new TextBox
        {
            Text = value.ToString(CultureInfo.InvariantCulture),
            BorderBrush = new SolidColorBrush(color),
            Margin = new Thickness(0, 0, 0, 0)
        };
        ToolTip.SetTip(textBox, tooltip);
        return textBox;
    }

    private static TextBlock CreateDimensionLabel(string label, Color color)
    {
        return new TextBlock
        {
            Text = label,
            Width = 15,
            VerticalAlignment = VerticalAlignment.Center,
            TextAlignment = TextAlignment.Center,
            Background = new SolidColorBrush(color),
        };
    }

    private void UpdateVectorComponent(
        object? sender,
        object targetObject
    )
    {
        if (sender == null)
        {
            return;
        }

        if (
            float.TryParse(
                ((TextBox)sender).Text,
                NumberStyles.Any,
                CultureInfo.InvariantCulture,
                out var component
            )
        )
        {
            var vector = ((Vector3D<float>?)memberInfo.GetValue(targetObject)) ?? Vector3D<float>.Zero;
            vector = new Vector3D<float>(
                xBox == sender ? component : vector.X,
                yBox == sender ? component : vector.Y,
                zBox == sender ? component : vector.Z
            );
            memberInfo.SetValue(targetObject, vector);
        }
    }
}
