namespace AtomicMassa.Engine.Core.Camera;

/// <summary>
/// Represents a camera node that can be used to view a scene from a specific perspective.
/// </summary>
[NodeContextMenu]
public class CameraNode : Node, ICamera
{
    /// <summary>
    /// Gets or sets a value indicating whether to use perspective projection.
    /// </summary>
    public bool UsePerspective { get; set; } = true;

    /// <summary>
    /// Gets or sets the distance to the near clipping plane of the camera's view frustum.
    /// </summary>
    public float NearPlane
    {
        get => nearPlane;
        set => nearPlane = Math.Max(value, float.Epsilon);
    }

    /// <summary>
    /// Gets or sets the distance to the far clipping plane of the camera's view frustum.
    /// </summary>
    public float FarPlane
    {
        get => farPlane;
        set => farPlane = Math.Max(value, float.Epsilon);
    }

    /// <summary>
    /// Gets or sets the pitch angle (in radians) of the camera's orientation.
    /// The pitch angle controls the vertical rotation of the camera.
    /// </summary>
    public float Pitch
    {
        get => Transform.Rotation.X;
        set
        {
            Transform.SetRotationX(Math.Clamp(value, -pitchClamp, pitchClamp));
            Transform.Rotation = Transform.Rotation with { X = (Math.Clamp(value, -pitchClamp, pitchClamp)) };
            UpdateVectors();
        }
    }

    /// <summary>
    /// Gets or sets the yaw angle (in radians) of the camera's orientation.
    /// The yaw angle controls the horizontal rotation of the camera.
    /// </summary>
    public float Yaw
    {
        get => Transform.Rotation.Y;
        set
        {
            Transform.SetRotationX(value);
            UpdateVectors();
        }
    }

    /// <summary>
    /// Gets or sets the frustum value, which represents the field of view in degrees.
    /// </summary>
    public float Frustum
    {
        get => frustum;
        set
        {
            frustum = value;
            UpdateOrtho();
        }
    }

    /// <summary>
    /// Gets the target point that the camera is looking at in 3D space.
    /// </summary>
    public Vector3D<float> Target => target;

    private float aspect = 1;
    private float left = -40;
    private float right = 40;
    private float bottom = 40;
    private float top = -40;
    private float nearPlane = 0.01f;
    private float farPlane = 100f;
    private Vector3D<float> target;
    private float frustum;
    private static Vector3D<float> GlobalUp => Vector3D<float>.UnitY;

    private const float pitchClamp = 89.99f * Mathf.degreesToRadians;
    private float fieldOfView = 40;

    #region ICamera

    /// <inheritdoc/>
    [JsonIgnore]
    public float AspectRatio => (left - right) / (top - bottom);

    /// <inheritdoc/>
    public float FieldOfView
    {
        get => fieldOfView;
        set => fieldOfView = Math.Clamp(value, float.Epsilon, Scalar<float>.Pi - float.Epsilon);
    }

    /// <inheritdoc/>
    [JsonIgnore, HideInEditor]
    public Vector3D<float> Position
    {
        get => Transform.Position;
        set => Transform.Position = value;
    }

    /// <inheritdoc/>
    [JsonIgnore, HideInEditor]
    public Vector3D<float> Front { get; private set; } = Vector3D<float>.UnitZ;

    /// <inheritdoc/>
    [JsonIgnore, HideInEditor]
    public Vector3D<float> Right { get; private set; } = Vector3D<float>.UnitX;

    /// <inheritdoc/>
    [JsonIgnore, HideInEditor]
    public Vector3D<float> Up { get; private set; }

    /// <inheritdoc/>
    public Vector4D<float> FrontVector4 => new(Front.X, Front.Y, Front.Z, 0f);

    /// <inheritdoc/>
    public Matrix4X4<float> GetInverseViewMatrix()
    {
        _ = Matrix4X4.Invert(GetViewMatrix(), out var ret);
        return ret;
    }

    /// <inheritdoc/>
    public Matrix4X4<float> GetProjectionMatrix() =>
        UsePerspective
            ? Matrix4X4.CreatePerspectiveFieldOfView(FieldOfView, aspect, NearPlane, FarPlane)
            : Matrix4X4.CreateOrthographicOffCenter(left, right, bottom, top, NearPlane, FarPlane);

    /// <inheritdoc/>
    public Matrix4X4<float> GetViewMatrix() =>
        UsePerspective
            ? Matrix4X4.CreateLookAt(Position, target, -Up)
            : Matrix4X4.CreateLookAt(Position, Position + Front, Up);

    /// <inheritdoc/>
    public Vector2D<float> Project(Vector3D<float> mouse3d)
    {
        var vec = new Vector4D<float>(mouse3d.X, mouse3d.Y, mouse3d.Z, 1.0f);
        vec = Vector4D.Transform(vec, GetViewMatrix());
        vec = Vector4D.Transform(vec, GetProjectionMatrix());
        if (Math.Abs(vec.W) > float.Epsilon)
        {
            vec /= vec.W;
        }

        return new Vector2D<float>(vec.X, vec.Y);
    }

    /// <inheritdoc/>
    public Vector3D<float> UnProject(Vector2D<float> mouse2d)
    {
        var vec = new Vector4D<float>(mouse2d.X, mouse2d.Y, 0f, 1.0f);
        _ = Matrix4X4.Invert(GetViewMatrix(), out var viewInv);
        _ = Matrix4X4.Invert(GetProjectionMatrix(), out var projInv);
        vec = Vector4D.Transform(vec, projInv);
        vec = Vector4D.Transform(vec, viewInv);
        if (Math.Abs(vec.W) > float.Epsilon)
        {
            vec /= vec.W;
        }

        return new Vector3D<float>(vec.X, vec.Y, vec.Z);
    }

    #endregion ICamera

    /// <summary>
    /// Pans the camera by a specified vector difference in 3D space.
    /// </summary>
    /// <param name="vStart">The starting point of the pan.</param>
    /// <param name="vStop">The ending point of the pan.</param>
    public void Pan(Vector3D<float> vStart, Vector3D<float> vStop)
    {
        var vdiff = vStart - vStop;
        Position += UsePerspective ? vdiff * 2f : vdiff;
        if (UsePerspective)
        {
            target += vdiff * 2f;
        }
    }

    /// <summary>
    /// Resizes the camera's aspect ratio based on the width and height of the viewport.
    /// </summary>
    /// <param name="wp">The width of the viewport.</param>
    /// <param name="hp">The height of the viewport.</param>
    public void Resize(uint wp, uint hp)
    {
        aspect = (float)wp / hp;
        UpdateVectors();
    }

    /// <summary>
    /// Creates a new CameraNode with a perspective projection.
    /// </summary>
    /// <returns>A new CameraNode instance.</returns>
    public static CameraNode CreateCameraPerspective()
    {
        var gameObj = new CameraNode();
        gameObj.Load(true, Vector2D<int>.Zero, 45f);
        return gameObj;
    }

    /// <summary>
    /// Creates a new CameraNode with an orthogonal projection.
    /// </summary>
    /// <returns>A new CameraNode instance.</returns>
    public static CameraNode CreateCameraOrthogonal()
    {
        var gameObj = new CameraNode();
        gameObj.Load(false, Vector2D<int>.Zero, 4f);

        return gameObj;
    }

    /// <summary>
    /// Loads camera settings.
    /// </summary>
    /// <param name="usePerspective">Whether to use perspective projection.</param>
    /// <param name="frameBufferSize">Frame buffer size.</param>
    /// <param name="fovDegrees">Field of view in degrees.</param>
    public void Load(bool usePerspective, Vector2D<int> frameBufferSize, float fovDegrees)
    {
        UsePerspective = usePerspective;
        NearPlane = UsePerspective ? 0.01f : -20f;
        FarPlane = UsePerspective ? 100f : 20f;
        Up = GlobalUp;
        FieldOfView = Scalar.DegreesToRadians(fovDegrees);
        Frustum = fovDegrees;

        Resize((uint)frameBufferSize.X, (uint)frameBufferSize.Y);
    }

    /// <summary>
    /// Updates the camera's front, right, and up vectors based on the current pitch and yaw.
    /// </summary>
    private void UpdateVectors()
    {
        Front = Vector3D.Normalize(
            new Vector3D<float>
            {
                X = MathF.Cos(Pitch) * MathF.Cos(Yaw),
                Y = MathF.Sin(Pitch),
                Z = MathF.Cos(Pitch) * MathF.Sin(Yaw)
            }
        );
        Right = Vector3D.Normalize(Vector3D.Cross(Front, GlobalUp));
        Up = Vector3D.Normalize(Vector3D.Cross(Right, Front));

        UpdateOrtho();
    }

    /// <summary>
    /// Updates the orthographic projection parameters based on the current frustum value.
    /// </summary>
    private void UpdateOrtho()
    {
        // Vulkan does top = negative
        left = Frustum * aspect / -2f;
        right = Frustum * aspect / 2f;
        top = Frustum / -2f;
        bottom = Frustum / 2f;
    }
}
