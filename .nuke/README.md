# Nuke Build system

In order to replicate the same building steps whenever it's in your local pc, in the cloud or anywhere in between, we use the [Nuke](https://nuke.build/) system to abstract.

Check our [guide](../docs/Building-from-source.md) how to build it.
