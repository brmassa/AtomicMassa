using Codeuctivity;

namespace AtomicMassa.Editor.Build.Settings;

/// <inheritdoc cref="IAppSettings"/>
public class BuildAppSettings : AmObject, IBuildAppSettings
{
    /// <inheritdoc/>
    public string? AssetJsonAbsolutePath { get; set; }

    /// <inheritdoc/>
    public string ProjectAbsoluteDir => Path.GetDirectoryName(AssetJsonAbsolutePath)!;

    /// <inheritdoc/>
    public string AssetsAbsoluteDir => Path.Combine(ProjectAbsoluteDir, "Assets");

    /// <inheritdoc/>
    public string CacheAbsoluteDir => Path.Combine(ProjectAbsoluteDir, ".Cache");

    /// <inheritdoc/>
    public string CacheSourceRelativeDir => Path.GetRelativePath(Path.Combine(CacheAbsoluteDir, SourceSubDir), Path.Combine(ProjectAbsoluteDir, SourceSubDir));

    /// <inheritdoc/>
    public string SourceSubDir => "Source";

    /// <inheritdoc/>
    public string ExportSourceSubDir => "Export";

    /// <inheritdoc/>
    public string? Title { get; set; }

    /// <inheritdoc/>
    public string? TitleToPathFriendly => Title?.SanitizeFilename(' ');

    /// <inheritdoc/>
    public IAppSettings Load(IAppSettings appSettings)
    {
        ArgumentNullException.ThrowIfNull(appSettings);

        AssetJsonAbsolutePath = appSettings.AssetJsonAbsolutePath;
        Title = appSettings.Title;
        return this;
    }

    /// <summary>
    /// List of external packages
    /// </summary>
    public string[] Packages => [
        "ImGui.NET",
        "Serilog",
        "Silk.NET",
        "System.Text.Json",
    ];

    /// <summary>
    /// List of external packages
    /// </summary>
    public (string, string)[] PackageReferences => [
        ("ImGui.NET", "1.91.0.1"),
        ("JeremyAnsel.Media.WavefrontObj", "3.0.39"),
        ("Microsoft.Extensions.Hosting", "8.0.1"),
        ("Serilog.Extensions.Hosting", "8.0.0"),
        ("Serilog.Sinks.Console", "6.0.0"),
        ("Serilog", "4.1.0"),
        ("Silk.NET.Input.Extensions", "2.21.0"),
        ("Silk.NET.Vulkan.Extensions.EXT", "2.21.0"),
        ("Silk.NET", "2.21.0"),
        ("System.Text.Json", "8.0.5"),
    ];

    /// <summary>
    /// List of internal engine packages
    /// </summary>
    public string[] InternalPackages => [
        "AtomicMassa/Engine/Attributes/AtomicMassa.Engine.Attributes",
        "AtomicMassa/Engine/Core/AtomicMassa.Engine.Core",
        "AtomicMassa/Engine/ImGui/AtomicMassa.Engine.ImGui"
    ];

    /// <summary>
    /// List of internal engine packages
    /// </summary>
    public (string, string)[] AtomicMassaPackages => [
        ("AtomicMassa/Engine/Attributes", "AtomicMassa.Engine.Attributes"),
        ("AtomicMassa/Engine/Core", "AtomicMassa.Engine.Core"),
        ("AtomicMassa/Engine/ImGui", "AtomicMassa.Engine.ImGui")
    ];

    /// <summary>
    /// The target Framework
    /// </summary>
    public string TargetFramework => "net8.0";

    /// <summary>
    /// The target SDK
    /// </summary>
    public string TargetSdk => "Microsoft.NET.Sdk";
}
