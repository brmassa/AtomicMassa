using System;

namespace AtomicMassa;

/// <summary>
///
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
public sealed class NumericUpDownAttribute : Attribute { }
