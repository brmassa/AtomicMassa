# AtomicMassa

```txt
 ______  __                                     
/\  _  \/\ \__                     __           
\ \ \L\ \ \ ,_\   ___     ___ ___ /\_\    ___   
 \ \  __ \ \ \/  / __`\ /' __` __`\/\ \  /'___\ 
  \ \ \/\ \ \ \_/\ \L\ \/\ \/\ \/\ \ \ \/\ \__/ 
   \ \_\ \_\ \__\ \____/\ \_\ \_\ \_\ \_\ \____\
    \/_/\/_/\/__/\/___/  \/_/\/_/\/_/\/_/\/____/
                                                
                                                
                                                
 /'\_/`\                                        
/\      \     __      ____    ____     __       
\ \ \__\ \  /'__`\   /',__\  /',__\  /'__`\     
 \ \ \_/\ \/\ \L\.\_/\__, `\/\__, `\/\ \L\.\_   
  \ \_\\ \_\ \__/.\_\/\____/\/\____/\ \__/.\_\  
   \/_/ \/_/\/__/\/_/\/___/  \/___/  \/__/\/_/  
                                                
                                                
```

> ***
> "The Game Engine You Never Knew You Needed" - mom
> ***

Are you tired of boring old game makers? Want to take your gaming experience to the next level? Look no further than **AtomicMassa**, the engine that will revolutionize the way you make digital entertainment.

So what are you waiting for? Download **NOW** and start creating your next big hit today!

> ***
> ⚠️ **WARNING**: It is currently in super ultra pre-alpha. If even compiles...
> ***

[![Latest release](https://gitlab.com/AtomicMassa/AtomicMassa/-/badges/release.svg)](https://gitlab.com/AtomicMassa/AtomicMassa)
![Pipepline](https://gitlab.com/AtomicMassa/AtomicMassa/badges/main/pipeline.svg?ignore_skipped=true)

## Introduction

* [🎯 Planned Features](#planned-features)
* [🚀 Getting Started](#getting-started)
* [🤝 Participate](#participate)
* [🔖 Licensing](#licensing)
* [🥇 Is it Better than... Unreal/Unity/Godot?](#is-it-better-than-unrealunitygodot)
* [🙌 We Support You](#we-support-you)
* [💖 You Support Us/Me](#you-support-usme)

## Planned Features

* Stunning 3D graphics that will blow your mind
* Intuitive controls that even your grandma can use
* A vast library of pre-made assets to jumpstart your development
* Customizable shaders for that extra oomph in your visuals
* A built-in physics engine to make your rag dolls fly
* And much, much more!

## Getting Started

Getting started is easy! We provide pre-compiled binaries for *Linux*, *Windows*, and *Mac*, so you can start using **AtomicMassa** right away. Simply head over to our releases page on and download the version that's right for you.

Check your [guide](./docs/Building-from-source.md) to build it from source!

## Participate

We're always looking for talented developers to help us improve **AtomicMassa**. Please check [how to contribute](./docs/Participate.md).

## Licensing

**AtomicMassa** is licensed under the **GNU General Public License version 3 (GPLv3)**, which means you can use it for commercial and non-commercial purposes. Just don't blame us if your app becomes too popular and you're suddenly rich and famous.

Check the full [License](LICENSE). Is a lot of technobabble, but it's what **ACTUALLY** counts.

And for stuff that we borrowed, check [Third Party Libraries](docs/ThirdPartyLibraries.md).

## Is it Better than... Unreal/Unity/Godot?

Short answer: yes. Totally. 100%.

We may not have all the fancy bells and whistles of those other game engines, but what we lack in flesh, we make up for in heart, soul, and still lack in flesh.

So if you're ready to join the **AtomicMassa** revolution and make something truly amazing, strap in and get ready for the ride of your life. It's gonna be wild, it's gonna be wacky, and most importantly, it's gonna be better than anything else out there.

## We Support You

If you need help with **AtomicMassa**, we've got you covered. Don't hesitate to contact our support team. They're available 24/7 to help you with any problems you may encounter. You will be able to reach us via our [Discord](https://discord.com/channels/1104509879269457982/1104509879865057342) server or [Matrix](https://matrix.to/#/!yyiCIgJgQDezXOaEgK:matrix.org?via=matrix.org) chat room.

Whether you're a seasoned developer or just getting started, we're here to help you make the most of **AtomicMassa** and create the best artistic vision possible.

## You Support Us/Me

**AtomicMassa** is (currently) a one-man project, developed and maintained by a dedicated developer. If you find **AtomicMassa** useful and would like to support its continued development, please consider making a financial contribution.

Your support will help to fund the ongoing development, including new features, bug fixes, and improvements. To make a donation, please visit our page and consider making a one-time donation:

[![Buy a coffee](https://storage.ko-fi.com/cdn/kofi2.png?v=3)](https://ko-fi.com/Y8Y3K59BX)

Your support is greatly appreciated and will help to ensure that **AtomicMassa** remains a cutting-edge game engine for years to come. Thank you!
