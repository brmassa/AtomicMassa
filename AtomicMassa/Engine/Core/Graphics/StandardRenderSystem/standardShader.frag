#version 460

const int uboLights = 10;

// Input attributes from the vertex shader
layout(location = 0) in vec3 fragColor;         // Fragment color
layout(location = 1) in vec3 fragPosWorld;      // Fragment position in world space
layout(location = 2) in vec3 fragNormalWorld;   // Fragment normal in world space

// Output color to the framebuffer
layout(location = 0) out vec4 outColor;

// Structure for point lights
struct PointLight {
	vec4 position;  // Light position (w component ignored)
	vec4 color;     // Light color and intensity (w component)
};

// Uniform buffer object containing global data
layout(set = 0, binding = 0) uniform GlobalUbo
{
	mat4 projection;                   // Projection matrix
	mat4 view;                         // View matrix
	vec4 front;                        // View direction
	vec4 ambientColor;                 // Ambient color and intensity
	PointLight pointLights[uboLights]; // Array of point lights
} ubo;

// Push constants for model and normal matrices
layout(push_constant) uniform Push 
{
	mat4 modelMatrix;     // Model transformation matrix
	mat4 normalMatrix;    // Normal transformation matrix
} push;

void main() {
    // Start with ambient lighting
	vec3 diffuseLight = ubo.ambientColor.xyz * ubo.ambientColor.w;
	vec3 specularLight = vec3(0.0);

    // Calculate normalized surface normal
	vec3 surfaceNormal = normalize(fragNormalWorld);

    // Calculate view direction
	vec3 viewDirection = -ubo.front.xyz;

    // Iterate over all point lights
	for (int i = 0; i < uboLights; i++)
	{
		PointLight light = ubo.pointLights[i];

        // Calculate direction to the light source
		vec3 directionToLight = light.position.xyz - fragPosWorld;

        // Calculate attenuation based on distance to light
		float distanceSquared = dot(directionToLight, directionToLight);
		float attenuation = 1.0 / distanceSquared;

		// Early exit if light is too dim
		if (attenuation * light.color.w < 0.01) continue;

		directionToLight = normalize(directionToLight);

        // Calculate diffuse lighting
		float cosAngIncidence = max(dot(surfaceNormal, directionToLight), 0);
		vec3 intensity = light.color.xyz * light.color.w * attenuation;
		diffuseLight += intensity * cosAngIncidence;

		// Calculate specular lighting using Blinn-Phong model
		vec3 halfAngle = normalize(directionToLight + viewDirection);
		float blinnTerm = dot(surfaceNormal, halfAngle);
		blinnTerm = clamp(blinnTerm, 0, 1);
		blinnTerm = pow(blinnTerm, 64.0);  // Specular exponent (shininess) is hardcoded to 64
		specularLight += intensity * blinnTerm;
	}

    // Combine diffuse and specular lighting with fragment color
	outColor = vec4(diffuseLight + specularLight, 1.0) * vec4(fragColor, 1.0);
}
