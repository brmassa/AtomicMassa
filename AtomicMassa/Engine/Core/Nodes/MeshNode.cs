namespace AtomicMassa.Engine.Core.Nodes;

/// <summary>
/// Represents a node in the engine that can render a mesh.
/// </summary>
[NodeContextMenu]
public class MeshNode : Node, IDisposable
{
    private ExtMeshShader extMeshShader = null!;

    /// <summary>
    /// Initializes the MeshNode with the specified parent and Vulkan instance.
    /// </summary>
    /// <param name="parent">The parent node, if any.</param>
    /// <param name="vulkan">The Vulkan instance used for rendering.</param>
    public override void Initialize(Node? parent, Vulkan vulkan)
    {
        ArgumentNullException.ThrowIfNull(vulkan);

        base.Initialize(parent, vulkan);

        _ = vulkan.Vk.TryGetDeviceExtension(
            vulkan.Device.Instance,
            vulkan.Device.VkDevice,
            out extMeshShader
        );
    }

    /// <summary>
    /// Calculates and returns the transformation matrix for the mesh node.
    /// </summary>
    /// <returns>The transformation matrix.</returns>
    public Matrix4X4<float> TransformationMatrix()
    {
        var c3 = MathF.Cos(Transform.Rotation.Z);
        var s3 = MathF.Sin(Transform.Rotation.Z);
        var c2 = MathF.Cos(Transform.Rotation.X);
        var s2 = MathF.Sin(Transform.Rotation.X);
        var c1 = MathF.Cos(Transform.Rotation.Y);
        var s1 = MathF.Sin(Transform.Rotation.Y);

        return new(
            Transform.Scale.X * ((c1 * c3) + (s1 * s2 * s3)),
            Transform.Scale.X * (c2 * s3),
            Transform.Scale.X * ((c1 * s2 * s3) - (c3 * s1)),
            0.0f,
            Transform.Scale.Y * ((c3 * s1 * s2) - (c1 * s3)),
            Transform.Scale.Y * (c2 * c3),
            Transform.Scale.Y * ((c1 * c3 * s2) + (s1 * s3)),
            0.0f,
            Transform.Scale.Z * (c2 * s1),
            Transform.Scale.Z * (-s2),
            Transform.Scale.Z * (c1 * c2),
            0.0f,
            Transform.Position.X,
            Transform.Position.Y,
            Transform.Position.Z,
            1.0f
        );
    }

    /// <summary>
    /// Binds the mesh node to a command buffer for rendering.
    /// </summary>
    /// <param name="_">The Vulkan command buffer.</param>
    public static void Bind(CommandBuffer _) { }

    /// <summary>
    /// Draws the mesh using the provided command buffer.
    /// </summary>
    /// <param name="commandBuffer">The Vulkan command buffer.</param>
    public void Draw(CommandBuffer commandBuffer)
    {
        extMeshShader.CmdDrawMeshTask(commandBuffer, 1, 1, 1);
    }

    /// <summary>
    /// Disposes of the MeshNode.
    /// </summary>
    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
