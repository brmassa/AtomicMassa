using System.Collections.Concurrent;

namespace AtomicMassa.Engine.Core.Nodes;

/// <summary>
/// Custom JSON converter for serializing and deserializing objects of type T (Object).
/// </summary>
/// <typeparam name="T">The type of objects to serialize and deserialize.</typeparam>
/// <remarks>
/// This JSON converter handles special cases, such as serializing and deserializing objects derived from Asset,
/// and allows dynamic serialization and deserialization of object properties and fields.
/// </remarks>
public class ObjectJsonSerializer<T> : JsonConverter<T>
    where T : AmObject
{
    private static readonly ConcurrentDictionary<Type, Dictionary<string, MemberInfo>> memberCache = new();

    /// <inheritdoc/>
    public override T Read(
        ref Utf8JsonReader reader,
        Type typeToConvert,
        JsonSerializerOptions options
    )
    {
        using var doc = JsonDocument.ParseValue(ref reader);
        var root = doc.RootElement;
        EnsureTypePropertyExists(root);
        var type = GetTypeFromProperty(root);
        var result = CreateInstance(type);

        ReadMembers(root, type, result, options);
        return result;
    }

    /// <inheritdoc/>
    public override void Write(Utf8JsonWriter writer, T value, JsonSerializerOptions options)
    {
        ArgumentNullException.ThrowIfNull(writer);
        ArgumentNullException.ThrowIfNull(value);

        writer.WriteStartObject();
        WriteTypeInformation(writer, value);

        var members = GetCachedMembers(value.GetType());
        WriteMembers(writer, value, members, options);

        writer.WriteEndObject();
    }

    private static void WriteTypeInformation(Utf8JsonWriter writer, T value)
    {
        writer.WriteString("__Type", value.GetType().AssemblyQualifiedName);
    }

    private static void WriteMembers(
        Utf8JsonWriter writer,
        T value,
        IDictionary<string, MemberInfo> members,
        JsonSerializerOptions options
    )
    {
        foreach (var item in members)
        {
            var member = item.Value;
            if (member is PropertyInfo property)
            {
                if (property.CanRead && property.CanWrite && IsMemberValid(property))
                {
                    var propValue = property.GetValue(value);
                    WriteMemberValue(
                        writer,
                        property.Name,
                        propValue,
                        property.PropertyType,
                        options
                    );
                }
            }
            else if (member is FieldInfo field)
            {
                if (IsMemberValid(field))
                {
                    var fieldValue = field.GetValue(value);
                    WriteMemberValue(writer, field.Name, fieldValue, field.FieldType, options);
                }
            }
        }
    }

    private static void WriteMemberValue(
        Utf8JsonWriter writer,
        string memberName,
        object? memberValue,
        Type memberType,
        JsonSerializerOptions options
    )
    {
        if (memberValue is Asset.Asset asset)
        {
            writer.WriteStartObject(memberName);
            writer.WriteString("__Type", asset.GetType().AssemblyQualifiedName);
            writer.WriteString("Id", asset.Id.ToString());
            writer.WriteEndObject();
        }
        else
        {
            writer.WritePropertyName(memberName);
            JsonSerializer.Serialize(writer, memberValue, memberType, options);
        }
    }

    private static void ReadMembers(JsonElement root, Type type, T result, JsonSerializerOptions options)
    {
        var members = GetCachedMembers(type);

        foreach (var prop in root.EnumerateObject())
        {
            if (prop.Name == "__Type")
            {
                continue;
            }

            ReadMember(prop, members, result, options);
        }
    }

    private static void ReadMember(
        JsonProperty prop,
        Dictionary<string, MemberInfo> members,
        T result,
        JsonSerializerOptions options
    )
    {
        if (members.TryGetValue(prop.Name, out var memberInfo))
        {
            object? value;
            Type memberType;

            if (memberInfo is PropertyInfo propertyInfo && propertyInfo.CanWrite)
            {
                memberType = propertyInfo.PropertyType;
            }
            else if (memberInfo is FieldInfo fieldInfo)
            {
                memberType = fieldInfo.FieldType;
            }
            else
            {
                return;
            }

            if (memberType == typeof(Asset.Asset) || memberType.IsSubclassOf(typeof(Asset.Asset)))
            {
                value = CreateAndSetAsset(prop, memberType);
            }
            else
            {
                value = JsonSerializer.Deserialize(prop.Value.GetRawText(), memberType, options);
            }

            if (memberInfo is PropertyInfo property)
            {
                property.SetValue(result, value);
            }
            else if (memberInfo is FieldInfo field)
            {
                field.SetValue(result, value);
            }
        }
    }

    private static bool IsMemberValid(MemberInfo member)
    {
        switch (member)
        {
            case PropertyInfo property:
                var getMethod = property.GetGetMethod(nonPublic: true);
                if (getMethod == null)
                {
                    return false;
                }

                return (
                        !getMethod.IsPublic
                        && property.GetCustomAttribute(typeof(JsonIncludeAttribute)) != null
                    )
                    || (
                        getMethod.IsPublic
                        && property.GetCustomAttribute(typeof(JsonIgnoreAttribute)) == null
                    );
            case FieldInfo field:
                return (
                        !field.IsPublic
                        && field.GetCustomAttribute(typeof(JsonIncludeAttribute)) != null
                    )
                    || (
                        field.IsPublic
                        && field.GetCustomAttribute(typeof(JsonIgnoreAttribute)) == null
                    );
            default:
                throw new ArgumentException("Member is not a property or field", nameof(member));
        }
    }

    private static Dictionary<string, MemberInfo> GetCachedMembers(Type type)
    {
        if (!memberCache.TryGetValue(type, out var members))
        {
            members = [];

            foreach (
                var prop in type.GetProperties(
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
                )
            )
            {
                members[prop.Name] = prop;
            }

            foreach (
                var field in type.GetFields(
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
                )
            )
            {
                members[field.Name] = field;
            }

            memberCache[type] = members;
        }
        return members;
    }

    private static void EnsureTypePropertyExists(JsonElement root)
    {
        if (!root.TryGetProperty("__Type", out _))
        {
            throw new JsonException("Type property is missing");
        }
    }

    private static Type GetTypeFromProperty(JsonElement root)
    {
        var typeProp = root.GetProperty("__Type");
        var typeString = typeProp.GetString() ?? throw new JsonException("Type property is null");
        var type = Type.GetType(typeString) ?? throw new JsonException($"Type not found: {typeProp}");
        return type;
    }

    private static T CreateInstance(Type type)
    {
        return Activator.CreateInstance(type) as T ?? throw new JsonException("Failed to create instance");
    }

    private static Asset.Asset CreateAndSetAsset(JsonProperty prop, Type _)
    {
        var assetTypeProp = prop.Value.GetProperty("__Type");
        var assetTypeString = assetTypeProp.GetString() ?? throw new JsonException("Type name is null");
        var actualAssetType = Type.GetType(assetTypeString) ?? throw new JsonException("Asset type not found");
        if (Activator.CreateInstance(actualAssetType) is not Asset.Asset asset)
        {
            throw new JsonException("Asset could not be created");
        }

        asset.Id = Guid.Parse(
            prop.Value.GetProperty("Id").GetString() ?? throw new JsonException("Id is null")
        );
        return asset;
    }
}
