namespace AtomicMassa;

internal static class Program
{
    private static void Main(string[] _)
    {
        // Setup Serilog
        Log.Logger = new LoggerConfiguration().WriteTo
            .Console(formatProvider: CultureInfo.InvariantCulture)
            .CreateLogger();

        // Create Host
        var host = Host.CreateDefaultBuilder()
            .ConfigureServices(
                (_, services) =>
                {
                    services
                        .AddSingleton(Log.Logger)

                        // Managers
                        .AddSingleton<WindowManager>()
                        .AddSingleton<InputManager>()
                        .AddSingleton<AssetDatabase>()
                        .AddSingleton<Vulkan>()
                        .AddSingleton<RendererManager>()

                        // App instance
                        .AddScoped<App>();
                }
            )
            .UseSerilog()
            .Build();

        Log.Logger.RestartTimer();
        Log.Logger.Lap("startup", "services registered");

        // Use our service
        using var serviceScope = host.Services.CreateScope();
        var services = serviceScope.ServiceProvider;

        try
        {
            var app = services.GetRequiredService<App>();
            app.Initialize();
        }
        catch (Exception ex)
        {
            Log.Fatal(ex.Message);
            throw;
        }

        // Close and flush the log
        Log.CloseAndFlush();
    }
}
