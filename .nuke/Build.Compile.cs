using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Utilities.Collections;
using Serilog;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace AtomicMassa.NUKE;

/// <summary>
/// This is the main build file for the project.
/// This partial is responsible for the build process.
/// </summary>
sealed partial class Build
{
    Target Clean => td =>
        td
            .Executes(() =>
            {
                SourceDirectory
                    .GlobDirectories("**/bin", "**/obj", "**/output")
                    .ForEach((path) => path.DeleteDirectory());
                TestProjectDirectory
                    .GlobDirectories("**/bin", "**/obj", "**/output")
                    .ForEach((path) => path.DeleteDirectory());
                PublishDirectory.DeleteDirectory();
                CoverageDirectory.DeleteDirectory();

                _ = DotNetClean();
            });

    Target Restore => td =>
        td
            .DependsOn(Clean)
            .Executes(() =>
            {
                _ = DotNetRestore(s => s.SetProjectFile(Solution));
            });

    Target Compile => td =>
        td
            .DependsOn(CompileShaders)
            .After(Restore)
            .Executes(() =>
            {
                Log.Debug("Configuration {Configuration}", Configuration);
                _ = DotNetBuild(
                    settings =>
                        settings
                            .SetNoLogo(true)
                            .SetProjectFile(Solution)
                            .SetConfiguration(Configuration)
                            .EnableNoRestore()
                );
            });
}
