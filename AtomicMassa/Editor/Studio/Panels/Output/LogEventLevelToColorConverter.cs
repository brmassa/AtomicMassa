using Avalonia.Data.Converters;
using Avalonia.Media;
using Serilog.Events;

namespace AtomicMassa.Editor.Studio.Panels.Output;

/// <summary>
/// Given a log event level, return a color
/// </summary>
public class LogEventLevelToColorConverter : IValueConverter
{
    /// <inheritdoc/>
    public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is not LogEventLevel logLevel)
        {
            return Brushes.Blue;
        }

        return logLevel switch
        {
            LogEventLevel.Information or LogEventLevel.Debug => Brushes.Blue,
            LogEventLevel.Warning => Brushes.Orange,
            LogEventLevel.Error or LogEventLevel.Fatal => Brushes.Red,
            _ => Brushes.Blue,
        };
    }

    /// <inheritdoc/>
    public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}
