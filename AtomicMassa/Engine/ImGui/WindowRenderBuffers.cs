// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

namespace AtomicMassa.Engine.ImGui;

internal unsafe struct WindowRenderBuffers
{
    public uint Index;
    public uint Count;
    public FrameRenderBuffer* FrameRenderBuffers;
};