namespace AtomicMassa.Editor.Studio.Panels.SceneTree;

/// <summary>
/// Content panel
/// </summary>
[Panel("Scene Tree")]
internal sealed partial class SceneTree : UserControl
{
    public SceneTree()
    {
        DataContext =
            App.Current.Services.GetRequiredService<SceneTreeViewModel>();

        InitializeComponent();
    }

    public void OnClick(object? sender, RoutedEventArgs _)
    {
        if (
            sender is TextBlock textBlock
            && DataContext is SceneTreeViewModel dataContext
            && textBlock.DataContext is SceneTreeNode sceneNode
        )
        {
            dataContext.SetSelectedObject(sceneNode.Node);
        }
    }

    private void TreeViewItem_RightClick(object? sender,
        PointerPressedEventArgs e)
    {
        if (e.GetCurrentPoint(null).Properties.IsRightButtonPressed &&
            sender is Control { DataContext: SceneTreeNode node } treeNode)
        {
            ShowContextMenu(node, treeNode);
        }
    }

    private void ShowContextMenu(SceneTreeNode nodeParent, Control control)
    {
        var contextMenu = new ContextMenu();
        var contextMenuItems = (DataContext as SceneTreeViewModel)!
            .GetClassesWithNodeContextMenuAttribute();

        // Create "New" submenu
        var newMenuItem = new MenuItem { Header = "New" };
        foreach (var type in contextMenuItems)
        {
            newMenuItem.Items.Add(CreateNewObjectMenuItem(nodeParent, type));
        }

        contextMenu.Items.Add(newMenuItem);


        // Add separator before edit operations
        contextMenu.Items.Add(new Separator());

        // Add edit operations
        var copyMenuItem = new MenuItem { Header = "Copy" };
        copyMenuItem.Click += (_, _) => CopyNode(nodeParent);
        contextMenu.Items.Add(copyMenuItem);

        var pasteMenuItem = new MenuItem { Header = "Paste" };
        pasteMenuItem.Click += (_, _) => PasteNode(nodeParent);
        pasteMenuItem.IsEnabled = HasCopiedNode();
        contextMenu.Items.Add(pasteMenuItem);

        var duplicateMenuItem = new MenuItem { Header = "Duplicate" };
        duplicateMenuItem.Click += (_, _) => DuplicateNode(nodeParent);
        contextMenu.Items.Add(duplicateMenuItem);

        var deleteMenuItem = new MenuItem { Header = "Delete" };
        deleteMenuItem.Click += (_, _) => DeleteNode(nodeParent);
        contextMenu.Items.Add(deleteMenuItem);

        contextMenu.Items.Add(new Separator());

        // Add ExpandAll menu item
        var expandAllMenuItem = new MenuItem { Header = "Expand All" };
        expandAllMenuItem.Click += (_, _) => ExpandAllNodes(nodeParent);
        contextMenu.Items.Add(expandAllMenuItem);

        // Add CollapseAll menu item
        var collapseAllMenuItem = new MenuItem { Header = "Collapse All" };
        collapseAllMenuItem.Click += (_, _) => CollapseAllNodes(nodeParent);
        contextMenu.Items.Add(collapseAllMenuItem);

        contextMenu.PlacementTarget = control;
        contextMenu.Open(control);
    }

    private static MenuItem CreateNewObjectMenuItem(SceneTreeNode nodeParent,
        Type type)
    {
        var menuItem = new MenuItem { Header = type.Name };
        menuItem.Click += (_, _) =>
        {
            var nodeNew = (Node)Activator.CreateInstance(type)!;
            nodeNew.Name = type.Name;

            // Update Node parent
            nodeNew.Parent = nodeParent.Node;
            nodeParent.Node.Children.Add(nodeNew);

            // Create and setup SceneTreeNode
            var sceneTreeNode = new SceneTreeNode(nodeNew)
                { Parent = nodeParent };
            nodeParent.Children.Add(sceneTreeNode);
        };
        return menuItem;
    }

    private static void ExpandAllNodes(SceneTreeNode node)
    {
        node.IsExpanded = true;
        foreach (var child in node.Children)
        {
            ExpandAllNodes(child);
        }
    }

    private static void CollapseAllNodes(SceneTreeNode node)
    {
        node.IsExpanded = false;
        foreach (var child in node.Children)
        {
            CollapseAllNodes(child);
        }
    }

    private SceneTreeNode? copiedNode;

    private void CopyNode(SceneTreeNode node)
    {
        copiedNode = node;
    }

    private bool HasCopiedNode() => copiedNode != null;

    private void PasteNode(SceneTreeNode targetNode)
    {
        if (copiedNode != null)
        {
            var clonedNode = DeepCloneNode(copiedNode,
                GetNextAvailableName(copiedNode.Name,
                    GetSiblingNames(targetNode)), targetNode);

            // Update Node parent
            clonedNode.Node.Parent = targetNode.Node;
            targetNode.Node.Children.Add(clonedNode.Node);

            // Update SceneTreeNode parent and add to children
            clonedNode.Parent = targetNode;
            targetNode.Children.Add(clonedNode);
        }
    }

    private void DuplicateNode(SceneTreeNode node)
    {
        if (node.Parent != null)
        {
            var clonedNode = DeepCloneNode(node,
                GetNextAvailableName(node.Name, GetSiblingNames(node.Parent)),
                node.Parent);

            // Update Node parent
            clonedNode.Node.Parent = node.Parent.Node;
            node.Parent.Node.Children.Add(clonedNode.Node);

            // Update SceneTreeNode parent and add to children
            clonedNode.Parent = node.Parent;
            node.Parent.Children.Add(clonedNode);
        }
        else if (DataContext is SceneTreeViewModel viewModel)
        {
            // Handle root node duplication
            var clonedNode = DeepCloneNode(node,
                GetNextAvailableName(node.Name,
                    viewModel.Items.Select(n => n.Name)), null);

            // For root nodes, no parent to update
            clonedNode.Node.Parent = null;

            viewModel.Items.Add(clonedNode);
        }
    }

    private static SceneTreeNode DeepCloneNode(SceneTreeNode originalNode,
        string newName, SceneTreeNode? parent)
    {
        // Create a new instance of the node's type with all its properties
        var clonedInternalNode =
            (Node)Activator.CreateInstance(originalNode.Node.GetType())!;
        clonedInternalNode.Name = newName;

        // Copy all public properties from the original node
        foreach (var property in originalNode.Node.GetType().GetProperties())
        {
            if (property.CanWrite && property.CanRead &&
                property.Name != "Children" && property.Name != "Parent")
            {
                var value = property.GetValue(originalNode.Node);
                property.SetValue(clonedInternalNode, value);
            }
        }

        // Create the new SceneTreeNode with the cloned internal node
        var clonedSceneNode = new SceneTreeNode(clonedInternalNode)
            { Parent = parent };

        // Recursively clone all children
        foreach (var childNode in originalNode.Children)
        {
            var clonedChild =
                DeepCloneNode(childNode, childNode.Name, clonedSceneNode);

            // Update Node parent for child
            clonedChild.Node.Parent = clonedInternalNode;
            clonedInternalNode.Children.Add(clonedChild.Node);

            // Update SceneTreeNode parent and add to children
            clonedChild.Parent = clonedSceneNode;
            clonedSceneNode.Children.Add(clonedChild);
        }

        return clonedSceneNode;
    }

    private static string GetNextAvailableName(string baseName,
        IEnumerable<string> existingNames)
    {
        // First, try to extract any existing number pattern
        var match =
            System.Text.RegularExpressions.Regex.Match(baseName,
                @"(.*?)\s*(\d+)?$");
        var nameWithoutNumber = match.Groups[1].Value.Trim();

        // If the name was empty (only had numbers), use the full base name
        if (string.IsNullOrEmpty(nameWithoutNumber))
        {
            nameWithoutNumber = baseName;
        }

        var number = 1;
        var newName = $"{nameWithoutNumber} {number}";

        while (existingNames.Contains(newName))
        {
            number++;
            newName = $"{nameWithoutNumber} {number}";
        }

        return newName;
    }

    private static IEnumerable<string> GetSiblingNames(SceneTreeNode parent)
    {
        return parent.Children.Select(child => child.Name);
    }

    private void DeleteNode(SceneTreeNode node)
    {
        if (node.Parent != null)
        {
            node.Parent.Children.Remove(node);
        }
        else if (DataContext is SceneTreeViewModel viewModel)
        {
            viewModel.Items.Remove(node);
        }
    }
}
