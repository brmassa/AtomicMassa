namespace AtomicMassa.Editor.Studio.Services.SelectedObject;

/// <summary>
/// Provides a way to get and set the selected object.
/// </summary>
public interface ISelectedObjectProvider<T>
{
    /// <summary>
    /// Gets the currently selected object, if any.
    /// </summary>
    T? SelectedObject { get; }

    /// <summary>
    /// Sets the currently selected object, from any where in the Studio.
    /// </summary>
    /// <param name="value"></param>
    void SetSelectedObject(T? value);

    /// <summary>
    /// Gets an observable that fires when the selected object changes,
    /// so the interested parties can update their UI.
    /// </summary>
    IObservable<EventPattern<ObjectChangedEventArgs<T>>> SelectedObjectChanged { get; }
}
