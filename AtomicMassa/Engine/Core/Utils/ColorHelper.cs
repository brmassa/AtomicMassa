namespace AtomicMassa.Engine.Core.Utils;

/// <summary>
/// Pre defined list of colors
/// </summary>
public static class Color4
{
#pragma warning disable CS1591

    public static Vector4D<float> White => new(1f, 1f, 1f, 1f);
    public static Vector4D<float> Black => new(0f, 0f, 0f, 1f);

    // my colors
    public static Vector4D<float> RoughGreen => new(0f, 0.6f, 0f, 1f);

    // main
    public static Vector4D<float> Red => new(1f, 0f, 0f, 1f);
    public static Vector4D<float> Green => new(0f, 1f, 0f, 1f);
    public static Vector4D<float> Blue => new(0f, 0f, 1f, 1f);

    // mix1
    public static Vector4D<float> Yellow => new(1f, 1f, 0f, 1f);
    public static Vector4D<float> Magenta => new(1f, 0f, 1f, 1f);
    public static Vector4D<float> Cyan => new(0f, 1f, 1f, 1f);
    public static Vector4D<float> Gray => new(0.5f, 0.5f, 0.5f, 1f);

    // others
    public static Vector4D<float> Pink => new(1f, 0.75f, 0.8f, 1f);
    public static Vector4D<float> Orange => new(1f, 0.65f, 0f, 1f);
    public static Vector4D<float> Purple => new(0.6f, 0.4f, 0.8f, 1f);
    public static Vector4D<float> Brown => new(0.6f, 0.4f, 0.2f, 1f);
    public static Vector4D<float> Beige => new(0.96f, 0.96f, 0.86f, 1f);
    public static Vector4D<float> Olive => new(0.5f, 0.5f, 0f, 1f);
    public static Vector4D<float> Maroon => new(0.5f, 0f, 0f, 1f);
    public static Vector4D<float> Navy => new(0f, 0f, 0.5f, 1f);
    public static Vector4D<float> Teal => new(0f, 0.5f, 0.5f, 1f);

    // others2
    public static Vector4D<float> Lime => new(0.75f, 1f, 0f, 1f);
    public static Vector4D<float> Turquoise => new(0.25f, 0.88f, 0.82f, 1f);
    public static Vector4D<float> Lavender => new(0.9f, 0.9f, 0.98f, 1f);
    public static Vector4D<float> Coral => new(1f, 0.5f, 0.31f, 1f);
    public static Vector4D<float> Salmon => new(0.98f, 0.5f, 0.45f, 1f);
    public static Vector4D<float> Peach => new(1f, 0.8f, 0.64f, 1f);
    public static Vector4D<float> Mint => new(0.6f, 1f, 0.8f, 1f);
    public static Vector4D<float> PowderBlue => new(0.69f, 0.88f, 0.9f, 1f);
    public static Vector4D<float> LightGray => new(0.83f, 0.83f, 0.83f, 1f);

    public static Vector4D<float> WhiteSmoke => new(0.96f, 0.96f, 0.96f, 1f);
}

public static class ColorHex
{
    public const uint white = 0xFFFFFFFF;
    public const uint black = 0x000000FF;

    // my colors
    public const uint roughGreen = 0x009900FF;

    // main
    public const uint red = 0xFF0000FF;
    public const uint green = 0x00FF00FF;
    public const uint blue = 0x0000FFFF;

    // mix1
    public const uint yellow = 0xFFFF00FF;
    public const uint magenta = 0xFF00FFFF;
    public const uint cyan = 0x00FFFFFF;
    public const uint gray = 0x808080FF;

    // others
    public const uint pink = 0xFFB5C5FF;
    public const uint orange = 0xFFA500FF;
    public const uint purple = 0x663399FF;
    public const uint brown = 0xA52A2AFF;
    public const uint beige = 0xF5F5DCFF;
    public const uint olive = 0x808000FF;
    public const uint maroon = 0x800000FF;
    public const uint navy = 0x000080FF;
    public const uint teal = 0x008080FF;

    // others2
    public const uint lime = 0xBFFF00FF;
    public const uint turquoise = 0x40E0D0FF;
    public const uint lavender = 0xE6E6FAFF;
    public const uint coral = 0xFF7F50FF;
    public const uint salmon = 0xFA8072FF;
    public const uint peach = 0xFFDAB9FF;
    public const uint mint = 0x98FB98FF;
    public const uint powderBlue = 0xB0E0E6FF;
    public const uint lightGray = 0xD3D3D3FF;

    public const uint whiteSmoke = 0xF5F5F5FF;
}

public static class Color3
{
    public static Vector3D<float> White => new(1f, 1f, 1f);
    public static Vector3D<float> Black => new(0f, 0f, 0f);

    // my colors
    public static Vector3D<float> RoughGreen => new(0f, 0.6f, 0f);

    // main
    public static Vector3D<float> Red => new(1f, 0f, 0f);
    public static Vector3D<float> Green => new(0f, 1f, 0f);
    public static Vector3D<float> Blue => new(0f, 0f, 1f);

    // mix1
    public static Vector3D<float> Yellow => new(1f, 1f, 0f);
    public static Vector3D<float> Magenta => new(1f, 0f, 1f);
    public static Vector3D<float> Cyan => new(0f, 1f, 1f);
    public static Vector3D<float> Gray => new(0.5f, 0.5f, 0.5f);

    // others
    public static Vector3D<float> Pink => new(1f, 0.75f, 0.8f);
    public static Vector3D<float> Orange => new(1f, 0.65f, 0f);
    public static Vector3D<float> Purple => new(0.6f, 0.4f, 0.8f);
    public static Vector3D<float> Brown => new(0.6f, 0.4f, 0.2f);
    public static Vector3D<float> Beige => new(0.96f, 0.96f, 0.86f);
    public static Vector3D<float> Olive => new(0.5f, 0.5f, 0f);
    public static Vector3D<float> Maroon => new(0.5f, 0f, 0f);
    public static Vector3D<float> Navy => new(0f, 0f, 0.5f);
    public static Vector3D<float> Teal => new(0f, 0.5f, 0.5f);

    // others2
    public static Vector3D<float> Lime => new(0.75f, 1f, 0f);
    public static Vector3D<float> Turquoise => new(0.25f, 0.88f, 0.82f);
    public static Vector3D<float> Lavender => new(0.9f, 0.9f, 0.98f);
    public static Vector3D<float> Coral => new(1f, 0.5f, 0.31f);
    public static Vector3D<float> Salmon => new(0.98f, 0.5f, 0.45f);
    public static Vector3D<float> Peach => new(1f, 0.8f, 0.64f);
    public static Vector3D<float> Mint => new(0.6f, 1f, 0.8f);
    public static Vector3D<float> PowderBlue => new(0.69f, 0.88f, 0.9f);
    public static Vector3D<float> LightGray => new(0.83f, 0.83f, 0.83f);

    public static Vector3D<float> WhiteSmoke => new(0.96f, 0.96f, 0.96f);

#pragma warning restore CS1591
}
