namespace AtomicMassa.Engine.Core.Nodes;

/// <summary>
/// Base for all engine objects.
/// </summary>
public class AmObject : IEquatable<AmObject>
{
    /// <summary>
    /// Object ID.
    /// </summary>
    [HideInEditor]
    public Guid Id { get; set; } = Guid.NewGuid();

    /// <summary>
    /// Load the Object content from the given path
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="absolutePath"></param>
    /// <returns></returns>
    public static T? Load<T>(string absolutePath)
        where T : AmObject
    {
        var jsonString = File.ReadAllText(absolutePath);
        return JsonSerializer.Deserialize<T>(jsonString, JsonOptions);
    }

    private static JsonSerializerOptions? jsonOptions;

    /// <summary>
    /// Defaul options for (de)serializing Objects
    /// </summary>
    public static JsonSerializerOptions JsonOptions
    {
        get
        {
            if (jsonOptions is null)
            {
                jsonOptions = new()
                {
                    IgnoreReadOnlyProperties = true,
                    IncludeFields = true,
                    WriteIndented = true
                };

                // Get all derived types of Object
                var derivedTypes = Assembly
                    .GetAssembly(typeof(AmObject))
                    ?.GetTypes()
                    .Where(t => t.IsSubclassOf(typeof(AmObject)))
                    .ToList();

                if (derivedTypes != null)
                {
                    // Add a new ObjectJsonSerializer for each derived type
                    foreach (var derivedType in derivedTypes)
                    {
                        var converterType = typeof(ObjectJsonSerializer<>).MakeGenericType(
                            derivedType
                        );
                        if (
                            Activator.CreateInstance(converterType)
                            is JsonConverter converterInstance
                        )
                        {
                            jsonOptions.Converters.Add(converterInstance);
                        }
                    }
                }
            }

            return jsonOptions;
        }
    }

    /// <summary>
    /// Indicates whether the current object is equal to another object of the same type.
    /// </summary>
    /// <param name="other">An object to compare with this object.</param>
    /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
    public bool Equals(AmObject? other)
    {
        if (other is null)
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return Id == other.Id;
    }

    /// <summary>
    /// Determines whether the specified object is equal to the current object.
    /// </summary>
    /// <param name="obj">The object to compare with the current object.</param>
    /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
    public override bool Equals(object? obj) => obj switch
    {
        AmObject other => Equals(other),
        _ => false,
    };

    /// <summary>
    /// Returns a hash code for the current object.
    /// </summary>
    /// <returns>A hash code for the current object.</returns>
    public override int GetHashCode() => Id.GetHashCode();

    /// <summary>
    /// Equality operator. Checks if two AmObject instances are equal.
    /// </summary>
    public static bool operator ==(AmObject? left, AmObject? right)
    {
        if (left is null)
        {
            return right is null;
        }
        return left.Equals(right);
    }

    /// <summary>
    /// Inequality operator. Checks if two AmObject instances are not equal.
    /// </summary>
    public static bool operator !=(AmObject? left, AmObject? right) => !(left == right);
}
