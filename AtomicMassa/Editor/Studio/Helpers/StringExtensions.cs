namespace AtomicMassa.Editor.Studio.Helpers;

/// <summary>
/// Helper methods for strings.
/// </summary>
public static class StringExtensions
{
    /// <summary>
    /// Converts a string to a readable name. Used for converting field names to labels.
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string ToReadableName(this string input)
    {
        if (string.IsNullOrEmpty(input))
        {
            return input;
        }

        if (input.StartsWith('_'))
        {
            input = input[1..];
        }
        else if (input.StartsWith("m_", StringComparison.InvariantCultureIgnoreCase))
        {
            input = input[2..];
        }

        var result = new StringBuilder();
        _ = result.Append(char.ToUpper(input[0], CultureInfo.CurrentCulture));

        for (var i = 1; i < input.Length; i++)
        {
            var currentChar = input[i];
            if (
                char.IsUpper(currentChar)
                && !char.IsWhiteSpace(input[i - 1])
                && !char.IsUpper(input[i - 1])
            )
            {
                _ = result.Append(' ');
            }
            _ = result.Append(currentChar);
        }

        return result.ToString();
    }

    /// <summary>
    /// Retrieves the loadable types from the given assembly.
    /// </summary>
    /// <param name="assembly">The assembly to retrieve loadable types from.</param>
    /// <returns>An enumerable collection of loadable types.</returns>
    public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly)
    {
        ArgumentNullException.ThrowIfNull(assembly);
        try
        {
            return assembly.GetTypes();
        }
        catch (ReflectionTypeLoadException e)
        {
            return e.Types.Where(t => t != null)!;
        }
    }
}
