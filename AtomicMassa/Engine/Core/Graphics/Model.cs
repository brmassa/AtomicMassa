using AtomicMassa.Types;

namespace AtomicMassa.Engine.Core.Graphics;

/// <summary>
/// Represents a 3D model for rendering using Vulkan graphics API.
/// </summary>
public class Model : IDisposable
{
    private readonly Vulkan vulkan;
    private Buffer vertexBuffer = null!;
    private readonly uint vertexCount;
    private readonly bool hasIndexBuffer;
    private Buffer indexBuffer = null!;
    private readonly uint indexCount;

    /// <summary>
    /// Initializes a new instance of the <see cref="Model"/> class.
    /// </summary>
    /// <param name="vulkan">The Vulkan context.</param>
    /// <param name="builder">A model builder for creating the model.</param>
    public Model(Vulkan vulkan, ModelBuilder builder)
    {
        this.vulkan = vulkan;
        vertexCount = (uint)builder.Vertices.Length;
        CreateVertexBuffers(builder.Vertices);
        indexCount = (uint)builder.Indices.Length;
        if (indexCount > 0)
        {
            hasIndexBuffer = true;
            CreateIndexBuffers(builder.Indices);
        }
    }

    private unsafe void CreateVertexBuffers(Vertex[] vertices)
    {
        var instanceSize = (ulong)Vertex.SizeOf();
        var bufferSize = instanceSize * (ulong)vertices.Length;

        using Buffer stagingBuffer =
            new(
                vulkan,
                instanceSize,
                vertexCount,
                BufferUsageFlags.TransferSrcBit,
                MemoryPropertyFlags.HostVisibleBit | MemoryPropertyFlags.HostCoherentBit
            );
        _ = stagingBuffer.Map();
        stagingBuffer.WriteToBuffer(vertices);

        vertexBuffer = new(
            vulkan,
            instanceSize,
            vertexCount,
            BufferUsageFlags.VertexBufferBit | BufferUsageFlags.TransferDstBit,
            MemoryPropertyFlags.DeviceLocalBit
        );

        vulkan.Device.CopyBuffer(stagingBuffer.VkBuffer, vertexBuffer.VkBuffer, bufferSize);
    }

    private unsafe void CreateIndexBuffers(uint[] indices)
    {
        var instanceSize = (ulong)Unsafe.SizeOf<uint>();
        var bufferSize = instanceSize * (ulong)indices.Length;

        using Buffer stagingBuffer =
            new(
                vulkan,
                instanceSize,
                indexCount,
                BufferUsageFlags.TransferSrcBit,
                MemoryPropertyFlags.HostVisibleBit | MemoryPropertyFlags.HostCoherentBit
            );
        _ = stagingBuffer.Map();
        stagingBuffer.WriteToBuffer(indices);

        indexBuffer = new(
            vulkan,
            instanceSize,
            indexCount,
            BufferUsageFlags.IndexBufferBit | BufferUsageFlags.TransferDstBit,
            MemoryPropertyFlags.DeviceLocalBit
        );

        vulkan.Device.CopyBuffer(stagingBuffer.VkBuffer, indexBuffer.VkBuffer, bufferSize);
    }

    /// <summary>
    /// Binds the model's vertex and index buffers to a Vulkan command buffer for rendering.
    /// </summary>
    /// <param name="commandBuffer">The Vulkan command buffer.</param>
    public unsafe void Bind(CommandBuffer commandBuffer)
    {
        var vertexBuffers = new Silk.NET.Vulkan.Buffer[] { vertexBuffer.VkBuffer };
        var offsets = new ulong[] { 0 };

        fixed (ulong* offsetsPtr = offsets)
        fixed (Silk.NET.Vulkan.Buffer* vertexBuffersPtr = vertexBuffers)
        {
            vulkan.Vk.CmdBindVertexBuffers(commandBuffer, 0, 1, vertexBuffersPtr, offsetsPtr);
        }

        if (hasIndexBuffer)
        {
            vulkan.Vk.CmdBindIndexBuffer(commandBuffer, indexBuffer.VkBuffer, 0, IndexType.Uint32);
        }
    }

    /// <summary>
    /// Draws the model using a Vulkan command buffer.
    /// </summary>
    /// <param name="commandBuffer">The Vulkan command buffer.</param>
    public void Draw(CommandBuffer commandBuffer)
    {
        if (hasIndexBuffer)
        {
            vulkan.Vk.CmdDrawIndexed(commandBuffer, indexCount, 1, 0, 0, 0);
        }
        else
        {
            vulkan.Vk.CmdDraw(commandBuffer, vertexCount, 1, 0, 0);
        }
    }

    /// <summary>
    /// Releases the resources held by the model.
    /// </summary>
    public unsafe void Dispose()
    {
        vertexBuffer.Dispose();
        indexBuffer.Dispose();
        GC.SuppressFinalize(this);
    }
}
